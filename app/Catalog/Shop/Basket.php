<?php

namespace App\Catalog\Shop;

use App\Model;
use App\Catalog\Product;
use App\Contracts\Userable;

class Basket extends Model
{

    use Userable;

    /**
     * @var string
     */
    protected $table = 'baskets';

    /**
     * @var array
     */
    protected $fillable = [
        'count',
        'user_id',
        'product_id',
    ];

    /**
     * @var bool
     */
    public $timestamps = false;


    /**
     * @return array
     */
    public static function rules(): array
    {
        return [
            'count' => 'required|min:1',
            'product_id' => 'required|exists:products,id',
        ];
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Product::class);
    }


    /**
     * @param bool $format
     * @return string
     */
    public function amount(bool $format = true): string
    {
        $amount = $this->product->getPriceDiscountAttribute() * $this->count;

        if ($format === true) {
            $amount = number_format($amount, 0, '.', ' ');
        }

        return $amount;
    }


    /**
     * @return string
     */
    public function linkDelete(): string
    {
        return url("/basket/{$this->id}/delete");
    }

}
