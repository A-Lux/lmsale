<?php

namespace App\Catalog\Shop;

use App\Model;
use App\Catalog\Product;

class Item extends Model
{

    /**
     * @var string
     */
    protected $table = 'items';

    /**
     * @var array
     */
    protected $fillable = [
        'order_id',
        'product_id',
        'count',
        'price',
        'discount',
    ];

    /**
     * @var bool
     */
    public $timestamps = false;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Order::class);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Product::class);
    }


    /**
     * @param bool $format
     * @return string
     */
    public function amount(bool $format = true): string
    {
        $amount = $this->price * $this->count;

        if ($format === true) {
            $amount = number_format($amount, 0, '.', ' ');
        }

        return $amount;
    }

}
