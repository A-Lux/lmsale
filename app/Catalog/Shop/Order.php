<?php

namespace App\Catalog\Shop;

use App\Model;
use App\Contracts\GUIDable;
use App\Contracts\Linkable;
use App\Contracts\Userable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model implements Linkable
{

    use Userable, GUIDable, SoftDeletes;

    /**
     * @var string
     */
	protected $dates = ['deleted_at'];
	
    protected $table = 'orders';

    /**
     * @var int
     */
    protected $amount = null;

    /**
     * @var array
     */
    protected $fillable = [
        'guid',
        'user_id',
        'delivery_name',
        'delivery_phone',
        'delivery_email',
        'delivery_comment',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'is_stored' => 'boolean',
    ];


    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeFilterBackend(Builder $builder): Builder
    {
        $builder->when(request('user'), function (Builder $builder): Builder {
            return $builder->where('user_id', request('user'));
        });

        return parent::scopeFilterBackend($builder);
    }


    /**
     * @return array
     */
    public static function rules(): array
    {
        return [
            'delivery_name' => 'required|min:2',
            'delivery_phone' => 'required|min:7|max:20',
            'delivery_email' => 'required|email',
        ];
    }


    /**
     * @return array
     */
    public static function messages(): array
    {
        return [
            'delivery_name' => 'имя',
            'delivery_phone' => 'контактный телефон',
            'delivery_email' => 'e-mail',
            'delivery_comment' => 'комментарий к заказу',
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Item::class);
    }


    /**
     * @param bool $format
     * @return string
     */
    public function amount(bool $format = true): string
    {
        if (is_null($this->amount)) {
            if (count($this->items)) {
                foreach ($this->items as $item) {
                    $this->amount += $item->amount(false);
                }
            } else {
                $this->amount = 0;
            }
        }

        if ($format === true) {
            return number_format($this->amount, 0, '.', ' ');
        }

        return $this->amount;
    }


    /**
     * @return string
     */
    public function link(): string
    {
        return url("/order/{$this->id}");
    }

}
