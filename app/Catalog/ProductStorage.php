<?php

namespace App\Catalog;

use Illuminate\Database\Eloquent\Builder;

/**
 * @version   1.0.1
 * @author    Astratyan Dmitry <astratyandmitry@gmail.com>
 * @copyright 2019, ArmenianBros. <i@armenianbros.com>
 */
class ProductStorage
{

    /**
     * @var array
     */
    protected $prices = [];

    /**
     * @var array
     */
    protected $discounts = [];

    /**
     * @var array
     */
    protected $discounts_order = null;


    public function __construct()
    {
        if (auth()->check()) {
            $this->loadPrices();
            $this->loadDiscounts();
        }
    }


    /**
     * @return void
     */
    protected function loadPrices(): void
    {
        $prices = Price::where('type_guid', auth()->user()->price_guid)->get();

        if (count($prices)) {
            foreach ($prices as $price) {
                $this->prices[$price->product_guid] = $price->price;
            }
        }
    }


    /**
     * @return void
     */
    protected function loadDiscounts(): void
    {
        $discounts = Discount::whereNull('order_sum')->where(function (Builder $builder): Builder {
            return $builder->where('user_guid', auth()->user()->guid)->orWhereNull('user_guid');
        })->where('started_at', '<=', date('Y-m-d'))->where('finished_at', '>=', date('Y-m-d'))->get();

        foreach ($discounts as $discount) {
            $key = ($discount->product_guid) ? $discount->product_guid : 'all';

            if (isset($this->discounts[$key])) {
                if ($this->discounts[$key] < $discount->discount) {
                    $this->discounts[$key] = $discount->discount;
                }
            } else {
                $this->discounts[$key] = $discount->discount;
            }
        }
    }


    /**
     * @return void
     */
    protected function loadOrderDiscounts(int $sum): void
    {
        $discounts = Discount::where('order_sum', '<=', $sum)->whereNotNull('order_sum')->where(function (Builder $builder): Builder {
            return $builder->where('user_guid', auth()->user()->guid)->orWhereNull('user_guid');
        })->where('started_at', '<=', date('Y-m-d'))->where('finished_at', '>=', date('Y-m-d'))->get();

        foreach ($discounts as $discount) {
            $key = ($discount->product_guid) ? $discount->product_guid : 'all';

            if (isset($this->discounts_order[$key])) {
                if ($this->discounts_order[$key] < $discount->discount) {
                    $this->discounts_order[$key] = $discount->discount;
                }
            } else {
                $this->discounts_order[$key] = $discount->discount;
            }
        }
    }


    /**
     * @param \App\Catalog\Product $product
     *
     * @return int
     */
    public function getPrice(Product $product): int
    {
        return (isset($this->prices[$product->guid])) ? $this->prices[$product->guid] : $product->price;
    }


    /**
     * @param \App\Catalog\Product $product
     *
     * @return int
     */
    public function getDiscount(Product $product): int
    {
        if (isset($this->discounts[$product->guid]) && isset($this->discounts['all'])) {
            return ($this->discounts[$product->guid] > $this->discounts['all']) ? $this->discounts[$product->guid] : $this->discounts['all'];
        }

        return isset($this->discounts[$product->guid]) ? $this->discounts[$product->guid] : (int)@$this->discounts['all'];
    }


    /**
     * @param \App\Catalog\Product $product
     *
     * @param int                  $sum
     *
     * @return int
     */
    public function getDiscountOrder(Product $product, int $sum = 0): int
    {
        if (is_null($this->discounts_order)) {
            $this->loadOrderDiscounts($sum);
        }

        if (isset($this->discounts_order[$product->guid]) && isset($this->discounts_order['all'])) {
            return ($this->discounts_order[$product->guid] > $this->discounts_order['all']) ? $this->discounts_order[$product->guid] : $this->discounts_order['all'];
        }

        return isset($this->discounts_order[$product->guid]) ? $this->discounts_order[$product->guid] : (int)@$this->discounts_order['all'];
    }


    /**
     * @param \App\Catalog\Product $product
     *
     * @return int
     */
    public function getPriceWithDiscount(Product $product): int
    {
        if ($price = $this->getPrice($product)) {
            if ($discount = $this->getDiscount($product)) {
                return (int)$price - ($price * ($discount / 100));
            }

            return $price;
        }

        return $product->price;
    }


    /**
     * @param \App\Catalog\Product $product
     *
     * @return int
     */
    public function getPriceWithDiscountOrder(Product $product): int
    {
        if ($price = $this->getPrice($product)) {
            if ($discount = $this->getDiscountOrder($product)) {
                return (int)$price - ($price * ($discount / 100));
            }

            return $price;
        }

        return $product->price;
    }

}
