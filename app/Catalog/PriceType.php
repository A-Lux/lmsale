<?php

namespace App\Catalog;

use App\Model;
use App\Contracts\GUIDable;

class PriceType extends Model
{

    use GUIDable;

    const DEFAULT_GUID = '3fc03793-c987-11e1-93a4-9439e5cc9820';

    /**
     * @var string
     */
    protected $table = 'price_types';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'guid',
    ];

    /**
     * @var bool
     */
    public $timestamps = false;


    /**
     * @return array
     */
    public static function rules(): array
    {
        return [
            'name' => 'required',
            'guid' => 'required',
        ];
    }

}
