<?php

namespace App\Catalog;

use App\Model;

class Price extends Model
{

    /**
     * @var string
     */
    protected $table = 'prices';

    /**
     * @var array
     */
    protected $fillable = [
        'product_guid',
        'type_guid',
        'price',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'price' => 'integer',
    ];

    /**
     * @var bool
     */
    public $timestamps = false;


    /**
     * @return array
     */
    public static function rules(): array
    {
        return [
            'product_guid' => 'required|exists:products,guid',
            'type_guid' => 'required|exists:product_price_types,guid',
            'price' => 'required|numeric',
        ];
    }


    /**
     * @param string $product_guid
     * @return Price|null
     */
    public static function getUserPrice(string $product_guid): ?Price
    {
        return Price::where('product_guid', $product_guid)->where('type_guid', auth()->user()->price_guid)->first();
    }

}
