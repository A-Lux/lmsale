<?php

namespace App\Catalog;

use App\Model;
use App\Contracts\GUIDable;
use App\Contracts\Linkable;
use App\Contracts\Sortable;
use App\Contracts\Activable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class Category extends Model implements Linkable
{

    use Sortable, Activable, GUIDable;

    /**
     * @var string
     */
    protected $table = 'categories';

    /**
     * @var Category|null
     */
    protected $parent = null;

    /**
     * @var Collection|null
     */
    protected $childs = null;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'guid',
        'parent_guid',
        'sort',
        'is_active',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'is_active' => 'boolean',
    ];

    /**
     * @var bool
     */
    public $timestamps = false;


    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeFilterBackend(Builder $builder): Builder
    {
        if ( ! request('id') &&  ! request('name')) {
            $builder->whereNull('parent_guid');
        }

        return parent::scopeFilterBackend($builder);
    }


    /**
     * @return array
     */
    public static function rules(): array
    {
        return [
            'name' => 'required|min:3',
            'sort' => 'required|min:1',
            'parent_guid' => 'nullable|exists:categories,guid',
        ];
    }


    /**
     * @return array
     */
    public static function messages(): array
    {
        return [
            'parent_guid' => 'родитель',
        ];
    }


    /**
     * @return Category
     */
    public function parent(): ?Category
    {
        if (is_null($this->parent)) {
            $this->parent = Category::where('guid', $this->parent_guid)->first();
        }

        return $this->parent;
    }


    /**
     * @return Collection|null
     */
    public function childs(): ?Collection
    {
        if (is_null($this->childs)) {
            $this->childs = Category::where('parent_guid', $this->guid)->get();
        }

        return $this->childs;
    }


    /**
     * @return string
     */
    public function link(): string
    {
        return url("/products/{$this->id}");
    }

}
