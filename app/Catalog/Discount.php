<?php

namespace App\Catalog;

use App\Model;

class Discount extends Model
{

    /**
     * @var string
     */
    protected $table = 'discounts';

    /**
     * @var array
     */
    protected $fillable = [
        'product_guid',
        'user_guid',
        'order_sum',
        'discount',
        'started_at',
        'finished_at',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'discount' => 'integer',
        'order_sum' => 'integer',
    ];

    /**
     * @var bool
     */
    public $timestamps = false;


    /**
     * @return array
     */
    public static function rules(): array
    {
        return [
            'product_guid' => 'required|exists:products,guid',
            'user_guid' => 'required|exists:users,guid',
            'discount' => 'required|numeric',
            'order_sum' => 'sometimes',
            'started_at' => 'required',
            'finished_at' => 'required',
        ];
    }

}
