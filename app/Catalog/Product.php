<?php

namespace App\Catalog;

use App\Model;
use App\Contracts\GUIDable;
use App\Contracts\Linkable;
use App\Contracts\Sortable;
use App\Contracts\Activable;
use Illuminate\Database\Eloquent\Builder;

class Product extends Model implements Linkable
{

    use Sortable, Activable, GUIDable;

    /**
     * @var string
     */
    protected $table = 'products';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'guid',
        'code',
        'price',
        'image',
        'description',
        'content',
        'about_properties',
        'about_application',
        'about_packing',
        'about_documentation',
        'about_analogs',
        'category_id',
        'is_line_top',
        'is_line_profy',
        'count_total',
        'count_order',
        'count_show',
        'sort',
        'has_no_sale',
        'is_active',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'is_line_top' => 'boolean',
        'is_line_profy' => 'boolean',
        'is_active' => 'boolean',
        'has_no_sale' => 'boolean',
    ];

    /**
     * @var array
     */
    protected $prices;


    /**
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopeFilterFrontend(Builder $builder): Builder
    {
        $builder->sorted();

        $builder->where('is_active', true);

        $builder->when(request('code'), function (Builder $builder): Builder {
            return $builder->where('code', request('code'));
        });

        return parent::scopeFilterFrontend($builder);
    }


    /**
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopeFilterBackend(Builder $builder): Builder
    {
        $builder->when(request('category'), function (Builder $builder): Builder {
            return $builder->where('category_id', request('category'));
        });

        return parent::scopeFilterBackend($builder);
    }


    /**
     * @param Builder  $builder
     * @param Category $category
     *
     * @return Builder
     */
    public function scopeCategory(Builder $builder, Category $category): Builder
    {
        if ( ! $category->parent_guid) {
            $childIds = $category->childs()->pluck('id');

            if (count($childIds)) {
                return $builder->whereIn('category_id', $category->childs()->pluck('id'));
            }

            return $builder->where('category_id', $category->id);
        }

        return $builder->where('category_id', $category->id);
    }


    /**
     * @return array
     */
    public static function rules(): array
    {
        return [
            'name' => 'required|min:3',
            'code' => 'required|min:1',
            'price' => 'required|min:1',
            'image' => 'required',
            'description' => 'required|min:30',
            'content' => 'required|min:50',
            'category_id' => 'required|exists:categories,id',
            'sort' => 'required|min:1',
        ];
    }


    /**
     * @return array
     */
    public static function messages(): array
    {
        return [
            'code' => 'артикул',
            'price' => 'цена',
            'about_properties' => 'свойства',
            'about_application' => 'применение',
            'about_packing' => 'фасовка',
            'about_documentation' => 'описание',
            'about_analogs' => 'аналоги',
            'category_id' => 'категория',
            'is_line_top' => 'линейка "TOP"',
            'is_line_profy' => 'линейка "Profy"',
        ];
    }


    /**
     * @return array
     */
    public function prices(): array
    {
        return $this->belongsTo(Category::class);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Category::class);
    }


    /**
     * @return int
     */
    public function getPriceDiscountAttribute(): int
    {
        return app('product-storage')->getPriceWithDiscount($this);
    }


    /**
     * @return int
     */
    public function getDiscountAttribute(): int
    {
        return app('product-storage')->getDiscount($this);
    }


    /**
     * @return string
     */
    public function link(): string
    {
        return url("/product/{$this->category->id}/{$this->id}");
    }

}
