<?php

namespace App\Console\Commands\WSDL;

use App\WSDL;
use Illuminate\Console\Command;

class WSDLSyncData extends Command
{

    /**
     * @var string
     */
    protected $signature = 'wsdl-sync:data {--truncate=false}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync data using SOAP WSLD';


    /**
     * @return void
     */
    public function handle(): void
    {
        $this->comment('Syncing...');

        if (WSDL::init()->DownloadDataForSearchSystem((bool)$this->option('truncate'))) {
            $this->info('Done!');
        } else {
            $this->error('Error!');
        }
    }

}
