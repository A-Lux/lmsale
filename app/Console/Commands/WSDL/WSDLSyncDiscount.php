<?php

namespace App\Console\Commands\WSDL;

use App\WSDL;
use Illuminate\Console\Command;

class WSDLSyncDiscount extends Command
{

    /**
     * @var string
     */
    protected $signature = 'wsdl-sync:discount {--truncate=true}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync data using SOAP WSLD';


    /**
     * @return void
     */
    public function handle(): void
    {
        $this->comment('Syncing...');

        if (WSDL::init()->DiscountsApply((bool)$this->option('truncate'))) {
            $this->info('Done!');
        } else {
            $this->error('Error!');
        }
    }

}
