<?php

namespace App\Console\Commands;

use App\WSDL;
use Illuminate\Console\Command;

class WSDLSyncDiscount extends Command
{

    /**
     * @var string
     */
    protected $signature = 'wsdl-sync:discount {--truncate=true}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync data using SOAP WSLD';


    /**
     * @return void
     */
    public function handle(): void
    {
        $optionTruncate = $this->option('truncate') == 'false' || ! $this->option('truncate') ? false : true;

        $this->comment('Syncing...');

        if (WSDL::init()->DiscountsApply($optionTruncate)) {
            $this->info('Done!');
        } else {
            $this->error('Error!');
        }
    }

}
