<?php

namespace App\Console\Commands;

use App\WSDL;
use Illuminate\Console\Command;

class WSDLSyncProducts extends Command
{

    /**
     * @var string
     */
    protected $signature = 'wsdl-sync:products {--new=true} {--truncate=false}';

    /**
     * @var string
     */
    protected $description = 'Sync products using SOAP WSLD';


    /**
     * @return void
     */
    public function handle(): void
    {
        $optionNew = $this->option('new') == 'false' || ! $this->option('new') ? false : true;
        $optionTruncate = $this->option('truncate') == 'false' || ! $this->option('truncate') ? false : true;

        $this->comment('Syncing...');

        $this->comment('...prices...');

        if ($prices = WSDL::init()->DownloadPricesOfNomenclatures()) {
            $this->info('...OK.');
        }

        $this->comment('...counts...');

        if ($counts = WSDL::init()->DownloadCountOfNomenclatures()) {
            $this->info('...OK.');
        }

        $this->comment('...products...');

        if (WSDL::init()->DownloadNomenclaturesOnSite($prices, $counts, $optionNew, $optionTruncate)
        ) {
            $this->info('...OK.');
        }

        $this->info('Done!');
    }

}
