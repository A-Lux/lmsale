<?php

namespace App\Console\Commands;

use App\WSDL;
use Illuminate\Console\Command;

class WSDLSyncData extends Command
{

    /**
     * @var string
     */
    protected $signature = 'wsdl-sync:data {--truncate=false}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync data using SOAP WSLD';


    /**
     * @return void
     */
    public function handle(): void
    {
        $optionTruncate = $this->option('truncate') == 'false' || ! $this->option('truncate') ? false : true;

        $this->comment('Syncing...');

        if (WSDL::init()->DownloadDataForSearchSystem($optionTruncate)) {
            $this->info('Done!');
        } else {
            $this->error('Error!');
        }
    }

}
