<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{

    /**
     * @var array
     */
    protected $commands = [
        'wsdl-sync:discount' => \App\Console\Commands\WSDLSyncDiscount::class,
        'wsdl-sync:data' => \App\Console\Commands\WSDLSyncData::class,
        'wsdl-sync:products' => \App\Console\Commands\WSDLSyncProducts::class,
    ];


    /**
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule): void
    {
         $schedule->command(\App\Console\Commands\WSDLSyncDiscount::class)->dailyAt('04:00');
         $schedule->command(\App\Console\Commands\WSDLSyncData::class)->dailyAt('04:10');
         $schedule->command(\App\Console\Commands\WSDLSyncProducts::class)->dailyAt('04:00');
    }


    /**
     * @return void
     */
    protected function commands(): void
    {
        require base_path('routes/console.php');
    }

}
