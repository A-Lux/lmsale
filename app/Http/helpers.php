<?php

/**
 * @param bool $boolean
 * @param bool $withTag
 */
function is_active(bool $boolean, bool $withTag = true)
{
    if ($boolean) {
        echo ($withTag) ? 'class="is-active"' : 'is-active';
    }
}

/**
 * @return array
 */
function paginateAppends(): array
{
    $parameters = [];

    if (count($_GET)) {
        foreach ($_GET as $key => $value) {
            if ($value !== "") {
                $parameters[$key] = $value;
            }
        }
    }

    return $parameters;
}
