<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotAdmin
{

    /**
     * @param Request $request
     * @param \Closure $next
     * @param string|null $guard
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ?string $guard = null)
    {
        $isAccessPage = (bool)($_SERVER['REQUEST_URI'] == '/backend/access');

        if (Auth::guard($guard)->guest()) {
            return redirect('/auth/signin?return=' . $_SERVER['REQUEST_URI']);
        }

        if (Auth::guard($guard)->user()->group_id != \App\Member\Group::ADMINISTRATOR) {
            if ( ! $isAccessPage) {
                return redirect('/backend/access');
            }
        } else {
            if ($isAccessPage) {
                return redirect('/backend');
            }
        }

        return $next($request);
    }

}
