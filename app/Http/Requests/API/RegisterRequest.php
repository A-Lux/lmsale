<?php

namespace App\Http\Requests\API;

/**
 * @version 1.0.1
 * @author Astratyan Dmitry <astratyandmitry@gmail.com>
 * @copyright 2018, ArmenianBros. <i@armenianbros.com>
 */
class RegisterRequest extends \App\Http\Requests\Request
{

    /**
     * @return bool
     */
    public function wantsJson(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'guid' => 'required',
            'iin' => 'required|max:12',
            'email' => 'required',
            'name' => 'required',
            'manager_id' => 'sometimes',
            'price_guid' => 'sometimes',
        ];
    }

}
