<?php

namespace App\Http\Requests\Backend;

use App\Information\Post;
use App\Http\Requests\Request;

class PostRequest extends Request
{

    /**
     * @return array
     */
    public function rules(): array
    {
        return $this->unique(Post::rules(), ['name'], 'posts');
    }

}
