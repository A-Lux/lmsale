<?php

namespace App\Http\Requests\Backend;

use App\Member\User;
use App\Http\Requests\Request;

class UserRequest extends Request
{

    /**
     * @return array
     */
    public function rules(): array
    {
        $rules = User::rules();

        if ($this->isMethod('POST')) {
            $rules['new_password'] = 'required|min:3';
        }

        return $this->unique($rules, ['email'], 'users');
    }


    /**
     * @return array
     */
    public function attributes(): array
    {
        return User::messages();
    }

}
