<?php

namespace App\Http\Requests\Backend;

use App\Catalog\Product;
use App\Http\Requests\Request;

class ProductRequest extends Request
{

    /**
     * @return array
     */
    public function rules(): array
    {
        return Product::rules();
    }


    /**
     * @return array
     */
    public function attributes(): array
    {
        return Product::messages();
    }

}
