<?php

namespace App\Http\Requests\Backend;

use App\Information\Page;
use App\Http\Requests\Request;

class PageRequest extends Request
{

    /**
     * @return array
     */
    public function rules(): array
    {
        return $this->unique(Page::rules(), ['name', 'slug'], 'pages');
    }

}
