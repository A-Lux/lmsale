<?php

namespace App\Http\Requests\Backend;

use App\Information\Slide;
use App\Http\Requests\Request;

class SlideRequest extends Request
{

    /**
     * @return array
     */
    public function rules(): array
    {
        return Slide::rules();
    }

}
