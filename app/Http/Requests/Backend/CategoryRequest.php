<?php

namespace App\Http\Requests\Backend;

use App\Catalog\Category;
use App\Http\Requests\Request;

class CategoryRequest extends Request
{

    /**
     * @return array
     */
    public function rules(): array
    {
        return $this->unique(Category::rules(), ['name'], 'categories');
    }


    /**
     * @return array
     */
    public function attributes(): array
    {
        return Category::messages();
    }

}
