<?php

namespace App\Http\Requests\Backend;

use App\Member\Group;
use App\Http\Requests\Request;

class GroupRequest extends Request
{

    /**
     * @return array
     */
    public function rules(): array
    {
        return $this->unique(Group::rules(), ['name'], 'groups');
    }


    /**
     * @return array
     */
    public function attributes(): array
    {
        return Group::messages();
    }

}
