<?php

namespace App\Http\Requests\Account\Settings;

use App\Http\Requests\Request;

class PasswordRequest extends Request
{

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'new_password' => 'required|min:6|confirmed',
            'current_password' => 'required|password',
        ];
    }


    /**
     * @return array
     */
    public function attributes(): array
    {
        return [
            'current_password' => 'текущий пароль',
            'new_password' => 'новый пароль',
            'new_password_confirmation' => 'повторите новый пароль',
        ];
    }

}
