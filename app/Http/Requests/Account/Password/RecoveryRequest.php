<?php

namespace App\Http\Requests\Account\Password;

class RecoveryRequest extends Request
{

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'new_password' => 'required|confirmed',
        ];
    }

}
