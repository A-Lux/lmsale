<?php

namespace App\Http\Requests\Account\Shop;

use App\Catalog\Shop\Basket;
use App\Http\Requests\Request;

class BasketRequest extends Request
{

    /**
     * @return array
     */
    public function rules(): array
    {
        return Basket::rules();
    }

}
