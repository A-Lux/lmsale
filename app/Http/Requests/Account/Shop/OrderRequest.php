<?php

namespace App\Http\Requests\Account\Shop;

use App\Catalog\Shop\Order;
use App\Http\Requests\Request;

class OrderRequest extends Request
{

    /**
     * @return array
     */
    public function rules(): array
    {
        if (auth()->user()->group_id == 2) {
            return [
                'user_id' => 'required|exists:users,id',
            ];
        }

        return Order::rules();
    }


    /**
     * @return array
     */
    public function attributes(): array
    {
        return Order::messages();
    }

}
