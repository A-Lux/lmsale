<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class Request extends FormRequest
{

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }


    /**
     * Return rules with attached unique rule to each attribute.
     *
     * @param array $rules
     * @param array $attributes
     * @param string $table
     * @return array
     */
    public function unique(array $rules, array $attributes, string $table): array
    {
        $isPatch = $this->isMethod('PATCH');

        foreach ($attributes as $attribute) {
            $unique = Rule::unique($table);

            if ($isPatch) {
                $unique->whereNot('id', $this->segment(3));
            }

            $rules[$attribute] .= "|{$unique}";
        }

        return $rules;
    }

}
