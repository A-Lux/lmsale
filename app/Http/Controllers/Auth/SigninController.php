<?php

namespace App\Http\Controllers\Auth;

use Illuminate\View\View;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class SigninController extends Controller
{

    use AuthenticatesUsers;

    /**
     * @var string
     */
    protected $redirectTo = '/';


    /**
     * @return View
     */
    public function form(): View
    {
        return view('auth.signin');
    }

    /**
     * @return string
     */
    public function redirectTo(): string
    {
        if (isset($_GET['return']) && ! empty($_GET['return'])) {
            return $_GET['return'];
        }

        return $this->redirectTo;
    }


    /**
     * @return string
     */
    public function username(): string
    {
        return 'iin';
    }
	
	public function test()
    {
        \Mail::raw('Hi, welcome user!', function ($message) {
		  $message->to('japananimetime@gmail.com')
			->subject('TEST');
		});
    }

}
