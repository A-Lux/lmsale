<?php

namespace App\Http\Controllers\Auth\Password;

use Illuminate\View\View;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class RecoveryController extends Controller
{

    use ResetsPasswords;

    /**
     * @var string
     */
    protected $redirectTo = '/auth/signin';


    /**
     * @param string $token
     * @return View
     */
    public function form(string $token): View
    {
        return view('auth.password.recovery', [
            'token' => $token,
        ]);
    }

}
