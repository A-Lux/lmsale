<?php

namespace App\Http\Controllers\Auth\Password;

use Illuminate\View\View;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class RestoreController extends Controller
{

    use SendsPasswordResetEmails;


    /**
     * @return View
     */
    public function form(): View
    {
        return view('auth.password.restore');
    }

}
