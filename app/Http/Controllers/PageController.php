<?php

namespace App\Http\Controllers;

use App\Information\Page;
use App\Information\Post;
use Illuminate\View\View;
use App\Information\Slide;

class PageController extends Controller
{

    /**
     * @return View
     */
    public function home(): View
    {
        return view('page.home', [
            'posts' => Post::sorted()->activated()->get(),
            'slides' => Slide::sorted()->activated()->get(),
        ]);
    }


    /**
     * @return View
     */
    public function card(Page $page): View
    {
        abort_if(! $page->isActive(), 404);

        $view = 'page.card';

        if ( ! $page->content) {
            if (file_exists(resource_path("page/static/{$page->slug}.blade.php"))) {
                abort(404);
            }

            $view = "page.static.{$page->slug}";
        }

        return view($view, [
            'page' => $page
        ]);
    }

}
