<?php

namespace App\Http\Controllers\Settings;

use Illuminate\View\View;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\Account\Settings\PasswordRequest;

class PasswordController extends Controller
{

    /**
     * @return View
     */
    public function form(): View
    {
        return view('account.settings.password');
    }


    /**
     * @param EmailRequest $request
     * @return RedirectResponse
     */
    public function process(PasswordRequest $request): RedirectResponse
    {
        $this->user->update([
            'password' => bcrypt($request->new_password),
        ]);

        return redirect()->to('/account');
    }

}
