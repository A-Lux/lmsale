<?php

namespace App\Http\Controllers\Settings;

use Illuminate\View\View;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\Account\Settings\EmailRequest;

class EmailController extends Controller
{

    /**
     * @return View
     */
    public function form(): View
    {
        return view('account.settings.email');
    }


    /**
     * @param EmailRequest $request
     * @return RedirectResponse
     */
    public function process(EmailRequest $request): RedirectResponse
    {
        $this->user->update([
            'email' => $request->email,
        ]);

        return redirect()->to('/account');
    }

}
