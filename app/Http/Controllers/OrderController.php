<?php

namespace App\Http\Controllers;

use App\WSDL;
use App\Member\User;
use Illuminate\View\View;
use App\Catalog\Shop\Item;
use App\Catalog\Shop\Order;
use Illuminate\Http\Request;
use App\Catalog\Shop\Basket;
use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\Account\Shop\OrderRequest;

class OrderController extends Controller
{

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function preorder(Request $request): RedirectResponse
    {
        if ( ! $request->has('basket')) {
            return redirect()->to(url('/basket'));
        }

        foreach ($request->basket as $id => $count) {
            $basket = Basket::find($id);

            if ($count < 1) {
                $basket->delete();
            } else {
                $basket->update([
                    'count' => $count,
                ]);
            }
        }

        return redirect()->to(url('/order'));
    }


    /**
     * @return View
     */
    public function form(): View
    {
        return view('order.form', [
            'user' => $this->user,
        ]);
    }


    /**
     * @param OrderRequest $request
     *
     * @return RedirectResponse
     */
    public function store(OrderRequest $request): RedirectResponse
    {
        $user = ($request->has('user_id')) ? User::find($request->user_id) : $this->user;
        $params = ( ! $request->has('user_id')) ? $request->all() : [
            'delivery_name' => $user->name,
            'delivery_phone' => $user->email,
            'delivery_email' => $user->email,
        ];

        $order = $user->orders()->create(array_merge(
            [
                'guid' => null,
                'status_id' => 1,
            ],
            $params
        ));

        $baskets = $this->user->baskets()->with('product')->get();
        $order_sum = (int)str_replace(' ', '', auth()->user()->basket()['amount']);

        foreach ($baskets as $basket) {
            if ($discount = app('product-storage')->getDiscountOrder($basket->product, $order_sum)) {
                if ( ! $basket->product->has_no_sale) {
                    $price = ($discount / 100) * $basket->product->price_discount;
                } else {
                    $discount = 0;
                    $price = $basket->product->price_discount;
                }
            } else {
                $discount = $basket->product->discount;
                $price = $basket->product->price_discount;
            }

            Item::create([
                'order_id' => $order->id,
                'product_id' => $basket->product->id,
                'price' => $price,
                'discount' => $discount,
                'count' => $basket->count,
            ]);
        }

        
		
        $order->guid = WSDL::init()->CreateOrderOfBuyer($order);
		if($order->guid != "Shortage of goods"){
			$this->user->baskets()->delete();
			$order->save();
		}
		else{			
			return redirect()->to('/basket#shortage');
		}

        return redirect()->to('/order/success');
    }


    /**
     * @return View
     */
    public function success(): View
    {
        return view('order.success');
    }


    /**
     * @param Order $order
     *
     * @return View
     */
    public function table(Order $order): View
    {
        abort_if(! $order->isOwned(), 404);

        return view('order.table', [
            'order' => $order,
            'baskets' => $order->items,
        ]);
    }


    /**
     * @param Order $order
     *
     * @return Response
     */
    public function document(Order $order)
    {
        $data = (new WSDL())->DownloadDocument($order->guid, (bool)\request()->query('created', 0));
        $data_filename = public_path("temporary.pdf");

        if ( ! empty($data) && ($fp = @fopen($data_filename, 'wb'))) {
            @fwrite($fp, $data);
            @fclose($fp);
        }

        unset($data);

        return response()->download($data_filename, "lm-sale.kz_order-{$order->id}.pdf");
    }

}
