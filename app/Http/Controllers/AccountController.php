<?php

namespace App\Http\Controllers;

use Illuminate\View\View;
use App\Catalog\Shop\Order;
use Illuminate\Http\RedirectResponse;

class AccountController extends Controller
{

    /**
     * @return View
     */
    public function profile(): View
    {
        return view('account.profile', [
            'user' => $this->user,
            'orders' => Order::owned()->with(['items'])->filterFrontend()->orderBy('id', 'desc')->get(),
        ]);
    }


    /**
     * @return RedirectResponse
     */
    public function signout(): RedirectResponse
    {
        \Auth::guard()->logout();

        request()->session()->flush();
        request()->session()->regenerate();

        return redirect()->to('/');
    }

}
