<?php

namespace App\Http\Controllers;

use App\Member\User;
use App\Catalog\Product;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\API\RegisterRequest;

class ApiController extends Controller
{

    /**
     * @var array
     */
    protected $response = [
        'status' => false,
    ];


    /**
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function register(RegisterRequest $request): JsonResponse
    {
        if ( ! $request->has('guid') || ! $request->has('email')) {
            return $this->setMessage('Wrong parameters')->getResponse();
        }

        if (User::where('guid', $request->guid, 'OR')->orWhere('iin', $request->iin)->exists()) {
            return $this->setMessage('GUID or IIN already exists')->getResponse();
        }

        $password = str_random(6);
        $user = User::create(array_merge($request->all(), [
            'group_id' => 3,
            'password' => bcrypt($password),
            'is_active' => true,
        ]));

        if ($user) {
            \Mail::to($user->email)->send(new \App\Mail\RegistrationMail($user, $password));
            \Mail::to('admin@lm-sale.kz')->send(new \App\Mail\RegistrationMail($user, $password));
        }

        return $this->setStatus((bool)$user)->getResponse();
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function recount(Request $request): JsonResponse
    {
        if ( ! $request->has('items')) {
            return $this->setMessage('Wrong parameters')->getResponse();
        }

        $items = json_decode($request->items);

        foreach ($items as $item) {
            list($guid, $count) = $item;

            if ($product = Product::where('guid', $guid)->first()) {
                $product->update(['count_total' => $count]);
            }
        }

        return $this->setStatus(true)->getResponse();
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function reprice(Request $request): JsonResponse
    {
        if ( ! $request->has('user_guid') || ! $request->has('price_guid')) {
            return $this->setMessage('Wrong parameters')->getResponse();
        }

        if ( ! $user = User::where('guid', $request->user_guid)->first()) {
            return $this->setMessage('User not found')->getResponse();
        }

        return $this->setStatus(
            $user->update(['price_guid' => $request->price_guid])
        )->getResponse();
    }


    /**
     * @return JsonResponse
     */
    protected function getResponse(): JsonResponse
    {
        return response()->json($this->response);
    }


    /**
     * @param bool $status
     * @return ApiController
     */
    protected function setStatus(bool $status): ApiController
    {
        $this->response['status'] = (bool)$status;

        return $this;
    }


    /**
     * @param string $message
     * @return ApiController
     */
    protected function setMessage(string $message): ApiController
    {
        $this->response['message'] = $message;

        return $this;
    }

}
