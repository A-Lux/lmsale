<?php

namespace App\Http\Controllers;

use App\Information\Post;
use Illuminate\View\View;

class PostController extends Controller
{

    /**
     * @return View
     */
    public function catalog(): View
    {
        return view('post.catalog', [
            'posts' => Post::filterFrontend()->paginate(10),
        ]);
    }


    /**
     * @param Post $post
     * @return View
     */
    public function card(Post $post): View
    {
        abort_if( ! $post->isActive(), 404);

        return view('post.card', [
            'post' => $post,
        ]);
    }

}
