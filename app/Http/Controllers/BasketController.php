<?php

namespace App\Http\Controllers;

use Illuminate\View\View;
use App\Catalog\Shop\Basket;
use App\Http\Requests\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\Account\Shop\BasketRequest;

class BasketController extends Controller
{

    /**
     * @return View
     */
    public function table(): View
    {
        $baskets = $this->user->baskets;
        $amount = 0;

        foreach ($baskets as $basket) {
            $amount += $basket->amount(false);
        }

        return view('basket.table', [
            'baskets' => $baskets,
            'amount' => number_format($amount, 0, '.', ' '),
        ]);
    }


    /**
     * @param Basket $id
     * @param Request $request
     */
    public function recount(Basket $id, Request $request)
    {
        if ($request->has('count')) {
            $basket = Basket::find($id);

            if ( ! $request->count || $request->count < 1) {
                $basket->delete();
            } else {
                $basket->update([
                    'count' => $request->count,
                ]);
            }
        }
    }


    /**
     * @param BasketRequest $request
     * @return JsonResponse
     */
    public function store(BasketRequest $request): RedirectResponse
    {
        if ($basket = Basket::owned()->where('product_id', $request->product_id)->first()) {
            $basket->update([
                'count' => $basket->count + $request->count,
            ]);
        } else {
            $this->user->baskets()->create($request->all());
        }

        return redirect()->to('/basket');
    }


    /**
     * @param Basket $basket
     * @return RedirectResponse
     */
    public function remove(Basket $basket): RedirectResponse
    {
        $basket->delete();

        return redirect()->to('/basket');
    }

}
