<?php

namespace App\Http\Controllers;

use App\Catalog\Shop\Basket;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class AjaxController extends Controller
{

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function basket_add(Request $request): JsonResponse
    {
        Basket::create($request->all());

        return response()->json($this->getBasketData());
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function basket_recount(Basket $basket, Request $request): JsonResponse
    {
        Basket::find($basket)->update([
            'count' => $request->count,
        ]);

        return response()->json($this->getBasketData());
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function basket_remove(Basket $basket): JsonResponse
    {
        Basket::find($basket)->delete();

        return response()->json($this->getBasketData());
    }


    /**
     * @return array
     */
    protected function getBasketData(): array
    {
        return $this->user->basket();
    }

}
