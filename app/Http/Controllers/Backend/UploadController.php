<?php

namespace App\Http\Controllers\Backend;

class UploadController extends Controller
{

    /**
     * @return string
     */
    public function tinymce(): string
    {
        $source = (new \App\Filesystem\Source('tinymce'));
        $file = (new \App\Filesystem\File($source))->save('image');

        return "<script>" .
            "$('.mce-reset .mce-btn').parent().find('input').val('{$file->getStoredPath()}');" .
            "$('.mce-reset button[role=presentation]').click()" .
            "</script>";
    }


    /**
     * @param string $path
     * @return string
     */
    public function file(string $path): string
    {
        $source = (new \App\Filesystem\Source($path));
        $file = (new \App\Filesystem\File($source))->save('file');

        return $file->getStoredPath();
    }


    /**
     * @param string $path
     * @return string
     */
    public function image(string $path): string
    {
        return (new \App\Filesystem\File((new \App\Filesystem\Source($path))))->save('file')->getStoredPath();
    }

}
