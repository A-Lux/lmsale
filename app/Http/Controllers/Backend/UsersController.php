<?php

namespace App\Http\Controllers\Backend;

use App\Member\User;
use App\Catalog\Shop\Order;
use App\Member\Group;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\Backend\UserRequest as Request;

class UsersController extends Controller
{

    /**
     * @var string
     */
    protected $redirectTo = 'backend/users';


    public function __construct()
    {
        $this->enject('group', Group::all());
        $this->enject('manager', User::where('group_id', Group::MANAGER)->get());
    }


    /**
     * @return View
     */
    public function index(): View
    {
        return view('backend.users.index', $this->enjected([
            'entities' => User::filterBackend()->paginate($this->itemsPerPage),
        ]));
    }


    /**
     * @return View
     */
    public function create(): View
    {
        return view('backend.users.create', $this->enjected());
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        User::create($this->withGUID($this->interactWithPassword($request->all())));

        return redirect()->to(url($this->redirectTo));
    }


    /**
     * @param int $id
     * @return View
     */
    public function edit(int $id): View
    {
        return view('backend.users.edit', $this->enjected([
            'entity' => User::find($id),
        ]));
    }


    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        $country = User::find($id);
        $country->update($this->interactWithPassword($request->all()));

        return redirect()->to(url($this->redirectTo));
    }


    /**
     * @param int $id
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id): RedirectResponse
    {
        User::find($id)->delete();
		Order::where('user_id', $id)->delete();
		
        return redirect()->to(url($this->redirectTo));
    }


    /**
     * @param array $attributes
     * @return array
     */
    protected function interactWithPassword(array $attributes): array
    {
        if (isset($attributes['new_password']) && ! empty($attributes['new_password'])) {
            $attributes['password'] = bcrypt($attributes['new_password']);
            unset($attributes['new_password']);
        }

        return $attributes;
    }

}
