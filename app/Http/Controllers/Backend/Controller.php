<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController
{

    /**
     * @var string
     */
    protected $redirectTo = '/backeend';

    /**
     * @var int
     */
    protected $itemsPerPage = 50;

    /**
     * @var array
     */
    protected $dependencies = [];


    /**
     * @param string $key
     * @param mixed $data
     */
    protected function enject(string $key, $data): void
    {
        $this->dependencies[$key] = $data;
    }


    /**
     * @param array $data
     * @return array
     */
    protected function enjected(array $data = []): array
    {
        return array_merge($data, ['dependencies' => $this->dependencies]);
    }


    /**
     * @param array $data
     * @return array
     */
    protected function withGUID(array $data = []): array
    {
        return array_merge(['guid' => null], $data);
    }

}
