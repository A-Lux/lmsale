<?php

namespace App\Http\Controllers\Backend;

use App\Settings;
use Illuminate\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

class SettingsController extends Controller
{

    /**
     * @return View
     */
    public function index(): View
    {
        return view('backend.settings.index', [
            'entity' => (new Settings())->getConfig(),
        ]);
    }


    /**
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        (new Settings())->setConfig($request->settings)->save();

        return redirect()->to(url('/backend/settings'));
    }

}
