<?php

namespace App\Http\Controllers\Backend;

use App\Member\Group;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\Backend\GroupRequest as Request;

class GroupsController extends Controller
{

    /**
     * @var string
     */
    protected $redirectTo = '/backend/groups';


    /**
     * @return View
     */
    public function index(): View
    {
        return view('backend.groups.index', [
            'entities' => Group::filterBackend()->paginate($this->itemsPerPage),
        ]);
    }


    /**
     * @return View
     */
    public function create(): View
    {
        return view('backend.groups.create');
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        Group::create($this->withGUID($request->all()));

        return redirect()->to(url($this->redirectTo));
    }


    /**
     * @param int $id
     * @return View
     */
    public function edit(int $id): View
    {
        return view('backend.groups.edit', [
            'entity' => Group::find($id),
        ]);
    }


    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        $country = Group::find($id);
        $country->update($request->all());

        return redirect()->to(url($this->redirectTo));
    }


    /**
     * @param int $id
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id): RedirectResponse
    {
        Group::find($id)->delete();

        return redirect()->to(url($this->redirectTo));
    }

}
