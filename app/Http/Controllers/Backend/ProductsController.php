<?php

namespace App\Http\Controllers\Backend;

use App\Catalog\Category;
use App\Catalog\Product;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\Backend\ProductRequest as Request;

class ProductsController extends Controller
{

    /**
     * @var string
     */
    protected $redirectTo = '/backend/products';


    public function __construct()
    {
        $this->enject('category', Category::orderBy('name', 'asc')->get());
    }


    /**
     * @return View
     */
    public function index(): View
    {
        return view('backend.products.index', $this->enjected([
            'entities' => Product::filterBackend()->paginate($this->itemsPerPage),
        ]));
    }


    /**
     * @return View
     */
    public function create(): View
    {
        return view('backend.products.create', $this->enjected());
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        Product::create($this->withGUID($request->all()));

        return redirect()->to(url($this->redirectTo));
    }


    /**
     * @param int $id
     * @return View
     */
    public function edit(int $id): View
    {
        return view('backend.products.edit', $this->enjected([
            'entity' => Product::find($id),
        ]));
    }


    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        $country = Product::find($id);
        $country->update($request->all());

        return redirect()->to(url($this->redirectTo));
    }


    /**
     * @param int $id
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id): RedirectResponse
    {
        Product::find($id)->delete();

        return redirect()->to(url($this->redirectTo));
    }

}
