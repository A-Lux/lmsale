<?php

namespace App\Http\Controllers\Backend;

use Illuminate\View\View;

class BackendController extends Controller
{

    /**
     * @return View
     */
    public function access(): View
    {
        return view('backend.access');
    }

}
