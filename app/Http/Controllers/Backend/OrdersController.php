<?php

namespace App\Http\Controllers\Backend;

use Illuminate\View\View;
use App\Catalog\Shop\Order;

class OrdersController extends Controller
{

    /**
     * @return View
     */
    public function index(): View
    {
        return view('backend.orders.index', [
            'entities' => Order::filterBackend()->paginate($this->itemsPerPage),
        ]);
    }


    /**
     * @param int $id
     * @return View
     */
    public function show(int $id): View
    {
        return view('backend.orders.show', [
            'entity' => Order::find($id),
        ]);
    }
	
	public function destroy(int $id): View
    {
        Order::find($id)->delete();

        return view('backend.orders.index', [
            'entities' => Order::filterBackend()->paginate($this->itemsPerPage),
        ]);
    }
}
