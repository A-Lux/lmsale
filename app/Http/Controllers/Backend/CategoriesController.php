<?php

namespace App\Http\Controllers\Backend;

use App\Catalog\Category;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\Backend\CategoryRequest as Request;

class CategoriesController extends Controller
{

    /**
     * @var string
     */
    protected $redirectTo = '/backend/categories';


    public function __construct()
    {
        $this->enject('parent', \App\Catalog\Category::orderBy('id')->whereNull('parent_guid')->get());
    }


    /**
     * @return View
     */
    public function index(): View
    {
        return view('backend.categories.index', $this->enjected([
            'entities' => Category::filterBackend()->paginate($this->itemsPerPage),
        ]));
    }


    /**
     * @return View
     */
    public function create(): View
    {
        return view('backend.categories.create', $this->enjected());
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        Category::create($this->withGUID($request->all()));

        return redirect()->to(url($this->redirectTo));
    }


    /**
     * @param int $id
     * @return View
     */
    public function edit(int $id): View
    {
        return view('backend.categories.edit', $this->enjected([
            'entity' => Category::find($id),
        ]));
    }


    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        $country = Category::find($id);
        $country->update($request->all());

        return redirect()->to(url($this->redirectTo));
    }


    /**
     * @param int $id
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id): RedirectResponse
    {
        Category::find($id)->delete();

        return redirect()->to(url($this->redirectTo));
    }

}
