<?php

namespace App\Http\Controllers\Backend;

use Illuminate\View\View;
use App\Information\Slide;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\Backend\SlideRequest as Request;

class SlidesController extends Controller
{

    /**
     * @var string
     */
    protected $redirectTo = '/backend/slides';


    /**
     * @return View
     */
    public function index(): View
    {
        return view('backend.slides.index', [
            'entities' => Slide::filterBackend()->paginate($this->itemsPerPage),
        ]);
    }


    /**
     * @return View
     */
    public function create(): View
    {
        return view('backend.slides.create');
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        Slide::create($request->all());

        return redirect()->to(url($this->redirectTo));
    }


    /**
     * @param int $id
     * @return View
     */
    public function edit(int $id): View
    {
        return view('backend.slides.edit', [
            'entity' => Slide::find($id),
        ]);
    }


    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        $country = Slide::find($id);
        $country->update($request->all());

        return redirect()->to(url($this->redirectTo));
    }


    /**
     * @param int $id
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id): RedirectResponse
    {
        Slide::find($id)->delete();

        return redirect()->to(url($this->redirectTo));
    }

}
