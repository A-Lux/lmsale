<?php

namespace App\Http\Controllers\Backend;

use App\Information\Post;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\Backend\PostRequest as Request;

class PostsController extends Controller
{

    /**
     * @var string
     */
    protected $redirectTo = '/backend/posts';


    /**
     * @return View
     */
    public function index(): View
    {
        return view('backend.posts.index', [
            'entities' => Post::filterBackend()->paginate($this->itemsPerPage),
        ]);
    }


    /**
     * @return View
     */
    public function create(): View
    {
        return view('backend.posts.create');
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        Post::create($this->withGUID($request->all()));

        return redirect()->to(url($this->redirectTo));
    }


    /**
     * @param int $id
     * @return View
     */
    public function edit(int $id): View
    {
        return view('backend.posts.edit', [
            'entity' => Post::find($id),
        ]);
    }


    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        $country = Post::find($id);
        $country->update($request->all());

        return redirect()->to(url($this->redirectTo));
    }


    /**
     * @param int $id
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id): RedirectResponse
    {
        Post::find($id)->delete();

        return redirect()->to(url($this->redirectTo));
    }

}
