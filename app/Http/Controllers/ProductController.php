<?php

namespace App\Http\Controllers;

use App\Catalog\Product;
use Illuminate\View\View;
use App\Catalog\Category;

class ProductController extends Controller
{

    /**
     * @return View
     */
    public function catalog(): View
    {
        return view('product.catalog', [
            'category' => null,
            'products' => Product::filterFrontend()->paginate(10),
        ]);
    }


    /**
     * @return View
     */
    public function search(): View
    {
        return view('product.catalog_search', [
            'category' => null,
            'products' => Product::filterFrontend()->paginate(10),
        ]);
    }


    /**
     * @param Category $category
     * @return View
     */
    public function filtered(Category $category): View
    {
        return view('product.catalog_category', [
            'category' => $category,
            'products' => Product::filterFrontend()->category($category)->paginate(10),
        ]);
    }


    /**
     * @param Category $category
     * @param Product $product
     * @return View
     */
    public function card(Category $category, Product $product): View
    {
        abort_unless($product->is_active, 404);

        return view('product.card', [
            'category' => $category,
            'product' => $product,
        ]);
    }

}
