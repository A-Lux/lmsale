<?php

namespace App\Member;

use App\Model;
use App\Contracts\GUIDable;
use App\Contracts\Activable;
use App\Contracts\Thumbnailable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract,
    Thumbnailable
{

    use Authenticatable, Authorizable, CanResetPassword, Notifiable, Activable, GUIDable, SoftDeletes;

    /**
     * @var string
     */
		
	protected $dates = ['deleted_at'];
		
    protected $table = 'users';

    /**
     * @var array|null
     */
    protected $basket = null;

    /**
     * @var array
     */
    protected $fillable = [
        'guid',
        'iin',
        'name',
        'email',
        'password',
        'group_id',
        'manager_id',
        'price_guid',
        'is_active',
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'is_active' => 'boolean',
    ];


    /**
     * @return array
     */
    public static function rules(): array
    {
        return [
            'iin' => 'required|max:12',
            'name' => 'required|min:2',
            'email' => 'required|email',
            'group_id' => 'required|exists:groups,id',
        ];
    }


    /**
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopeFilterBackend(Builder $builder): Builder
    {
        $builder->when(request('email'), function (Builder $builder): Builder {
            return $builder->where('email', 'LIKE', "%" . request('email') . "%");
        });

        $builder->when(request('group'), function (Builder $builder): Builder {
            return $builder->where('group_id', request('group'));
        });

        return parent::scopeFilterBackend($builder);
    }


    /**
     * @return array
     */
    public static function messages(): array
    {
        return [
            'name' => 'ФИО',
            'group_id' => 'группа',
            'price_guid' => 'тип цены',
        ];
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Group::class);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function baskets(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Catalog\Shop\Basket::class);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Catalog\Shop\Order::class);
    }


    /**
     * @return Collection
     */
    public function contragents(): Collection
    {
        return User::whereIn('manager_id', $this->id)->get();
    }


    /**
     * @return array
     */
    public static function thumbnailSizes(): array
    {
        return [100, 100];
    }


    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return in_array($this->group_id, [
            Group::ADMINISTRATOR,
            Group::MANAGER,
        ]);
    }


    /**
     * @return array
     */
    public function basket(): array
    {
        if (is_null($this->basket)) {
            $baskets = $this->baskets;
            $this->basket = ['total' => 0, 'amount' => 0];

            if (count($baskets)) {
                foreach ($baskets as $basket) {
                    $this->basket['total'] += 1;
                    $this->basket['amount'] += $basket->amount(false);
                }

                $this->basket['total'] = number_format($this->basket['total'], 0, '.', ' ');
                $this->basket['amount'] = number_format($this->basket['amount'], 0, '.', ' ');
            }
        }

        return $this->basket;
    }


    /**
     * @return bool
     */
    public function canBuy(): bool
    {
        return $this->is_active;
    }

}
