<?php

namespace App\Member;

use App\Model;
use App\Contracts\GUIDable;

class Group extends Model
{

    use GUIDable;

    const ADMINISTRATOR = 1;
    const MANAGER = 2;

    /**
     * @var string
     */
    protected $table = 'groups';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'guid',
    ];

    /**
     * @var bool
     */
    public $timestamps = false;


    /**
     * @return array
     */
    public static function rules(): array
    {
        return [
            'name' => 'required',
        ];
    }

}
