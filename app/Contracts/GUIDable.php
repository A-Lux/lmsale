<?php

namespace App\Contracts;

use App\GUID;

trait GUIDable
{

    /**
     * @param null|string $value
     */
    public function setGuidAttribute(?string $value): void
    {
        $this->attributes['guid'] = ( ! is_null($value)) ? $value : GUID::generate();
    }

}
