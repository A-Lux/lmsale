<?php

namespace App\Contracts;

use Illuminate\Database\Eloquent\Builder;

trait Activable
{

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return (bool)$this->is_active;
    }


    /**
     * @return bool
     */
    public function activate(): bool
    {
        $this->is_active = true;

        return $this->save();
    }


    /**
     * @return bool
     */
    public function deactivate(): bool
    {
        $this->is_active = false;

        return $this->save();
    }


    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeActivated(Builder $builder): Builder
    {
        return $builder->where('is_active', true);
    }


    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeUnactivated(Builder $builder): Builder
    {
        return $builder->where('is_active', false);
    }

}
