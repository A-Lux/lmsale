<?php

namespace App\Contracts;

use App\Member\User;
use Illuminate\Database\Eloquent\Builder;

trait Userable
{

    /**
     * @param Builder $builder
     * @param int|null $user_id
     * @return Builder
     */
    public function scopeOwned(Builder $builder, ?int $user_id = null): Builder
    {
        return $builder->where('user_id', (is_null($user_id)) ? auth()->user()->id : $user_id);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }


    /**
     * @param int|null $user_id
     * @return bool
     */
    public function isOwned(?int $user_id = null): bool
    {
        return $this->user_id == ((is_null($user_id)) ? auth()->user()->id : $user_id);
    }

}
