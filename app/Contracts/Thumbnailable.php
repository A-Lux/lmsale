<?php

namespace App\Contracts;

interface Thumbnailable
{

    /**
     * @return array
     */
    public static function thumbnailSizes(): array;

}
