<?php

namespace App\Contracts;

use Illuminate\Database\Eloquent\Builder;

trait Sortable
{

    /**
     * @param Builder $builder
     * @param string $order
     * @return Builder
     */
    public function scopeSorted(Builder $builder, string $order = 'DESC'): Builder
    {
        return $builder->orderBy('sort', $order);
    }

}
