<?php

namespace App\Contracts;

use App\Model;
use Illuminate\Database\Eloquent\Builder;

trait Entityable
{

    /**
     * @param Builder $builder
     * @param Model $model
     * @param int $id
     * @return Builder
     */
    public function scopeEntity(Builder $builder, Model $model, int $id): Builder
    {
        return $builder->where('entity_class', get_class($model))->where('entity_id', $id);
    }


    /**
     * @return Model
     */
    public function entity(): Model
    {
        return call_user_func_array([$this->entity_class, 'where'], ['id', $this->entity_id])->first();
    }

}
