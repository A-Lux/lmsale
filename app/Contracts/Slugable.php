<?php

namespace App\Contracts;

trait Slugable
{

    /**
     * @param string $value
     */
    public function setSlugAttribute(string $value): void
    {
        $this->attributes['slug'] = \php_rutils\RUtils::translit()->slugify($value);
    }


    /**
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }

}
