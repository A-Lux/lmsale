<?php

namespace App\Contracts;

interface Linkable
{

    public function link(): string;

}
