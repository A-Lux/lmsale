<?php

namespace App\Filesystem;

use Imagine\Image\Box;
use Imagine\Image\ImageInterface;

class Thumbnail extends Fileable
{

    /**
     * @param int $width
     * @param int $height
     * @return Thumbnail
     */
    public function make(File $file, $width, $height): Thumbnail
    {
        $this->generateFilename($file);

        $size = new Box($width, $height);

        app('orchestra.imagine')
            ->open(public_path(trim($file->getStoredPath(), '/')))
            ->thumbnail($size, ImageInterface::THUMBNAIL_OUTBOUND)
            ->save(public_path($this->source->getPath()) . '/' . $this->filename);

        return $this;
    }


    /**
     * @param File $file
     * @return string
     */
    protected function generateFilename(File $file): string
    {
        $storedPath = $file->getStoredPath();
        $explodedStoredPath = explode('/', $storedPath);
        $filename = $explodedStoredPath[count($explodedStoredPath) - 1];

        $extensionPosition = strrpos($filename, '.');
        $thumb = substr($filename, 0, $extensionPosition) . '.thumb' . substr($filename, $extensionPosition);

        return $this->filename = $thumb;
    }


    /**
     * @param \App\Model $model
     * @return string
     */
    public static function get(\App\Model $model): string
    {
        $filename = explode('.', $$model->imagestring);
        $extension = $filename[count($filename) - 1];
        $filename[count($filename) - 1] = '.thumb';
        $filename[] = $extension;

        return implode('.', $filename);
    }

}
