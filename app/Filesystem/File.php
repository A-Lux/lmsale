<?php

namespace App\Filesystem;

class File extends Fileable
{

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $extension;


    /**
     * @param string $attribute
     * @return void
     */
    protected function setFileData(string $attribute): void
    {
        $this->name = $this->originalName($attribute);
        $this->extension = $this->originalExtension($attribute);
        $this->filename = $this->generateFilename();
    }


    /**
     * @return array
     */
    public function getFileData(): array
    {
        return [
            $this->name,
            $this->extension,
        ];
    }


    /**
     * @param string $path
     * @return File
     */
    public function load(string $path): File
    {
        $exlodedPath = explode('/', $path);
        $this->filename = last($exlodedPath);
        $exlodedPath = explode('.', $this->filename);
        $this->extension = last($exlodedPath);
        unset($exlodedPath[count($exlodedPath) - 1]);
        $this->name = implode('.', $exlodedPath);

        return $this;
    }


    /**
     * @param string $attribute
     * @return File
     */
    public function save(string $attribute): File
    {
        $this->setFileData($attribute);

        request()->file($attribute)->move(
            public_path($this->source->getPath()),
            $this->filename
        );

        return $this;
    }


    /**
     * @param string $attribute
     * @return string
     */
    protected function originalName(string $attribute): string
    {
        $explodedName = explode('.', request()->file($attribute)->getClientOriginalName());
        unset($explodedName[count($explodedName) - 1]);

        return implode('.', $explodedName);
    }


    /**
     * @param string $attribute
     * @return string
     */
    protected function originalExtension(string $attribute): string
    {
        return request()->file($attribute)->getClientOriginalExtension();
    }


    /**
     * @return string
     */
    protected function generateFilename(): string
    {
        return "{$this->generateRandomString()}_{$this->name}.{$this->extension}";
    }

}
