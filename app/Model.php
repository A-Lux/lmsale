<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model as BaseModel;

class Model extends BaseModel
{

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeFilterFrontend(Builder $builder): Builder
    {
        return $builder;
    }


    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeFilterBackend(Builder $builder): Builder
    {
        $builder->orderBy('id', 'desc');

        $builder->when(request('id'), function (Builder $builder): Builder {
            return $builder->where('id', request('id'));
        });

        $builder->when(request('guid'), function (Builder $builder): Builder {
            return $builder->where('guid', 'LIKE', '%' . request('guid') . '%');
        });

        $builder->when(request('name'), function (Builder $builder): Builder {
            return $builder->where('name', 'LIKE', "%" . request('name') . "%");
        });

        $builder->when(request('active'), function (Builder $builder): Builder {
            return $builder->where('is_active', (bool) request('active'));
        });

        return $builder;
    }


    /**
     * @return array
     */
    public static function rules(): array
    {
        return [];
    }


    /**
     * @return array
     */
    public static function messages(): array
    {
        return [];
    }

}
