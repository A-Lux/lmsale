<?php

namespace App\Information;

use App\Model;
use App\Contracts\GUIDable;
use App\Contracts\Linkable;
use App\Contracts\Sortable;
use App\Contracts\Activable;
use App\Contracts\Thumbnailable;

class Post extends Model implements Linkable, Thumbnailable
{

    use Activable, Sortable, GUIDable;

    /**
     * @var string
     */
    protected $table = 'posts';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'guid',
        'description',
        'content',
        'image',
        'sort',
        'is_active',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'is_active' => 'boolean',
    ];


    /**
     * @return array
     */
    public static function rules(): array
    {
        return [
            'name' => 'required|min:3',
            'description' => 'required|min:30',
            'content' => 'required|min:50',
            'image' => 'required',
            'sort' => 'required|min:1',
        ];
    }


    /**
     * @return array
     */
    public static function thumbnailSizes(): array
    {
        return [200, 230];
    }


    /**
     * @return string
     */
    public function link(): string
    {
        return url("/post/{$this->id}");
    }

}
