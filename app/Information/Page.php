<?php

namespace App\Information;

use App\Model;
use App\Contracts\Linkable;
use App\Contracts\Slugable;
use App\Contracts\Activable;

class Page extends Model implements Linkable
{

    use Slugable, Activable;

    /**
     * @var string
     */
    protected $table = 'pages';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'content',
        'is_active',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'is_active' => 'boolean',
    ];


    /**
     * @return array
     */
    public static function rules(): array
    {
        return [
            'name' => 'required|min:3',
            'slug' => 'required|min:3',
            'content' => 'nullable',
        ];
    }


    /**
     * @param string $value
     */
    public function setSlugAttribute(string $value): void
    {
        $this->attributes['slug'] = \php_rutils\RUtils::translit()->slugify($value);
    }


    /**
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }


    /**
     * @return string
     */
    public function link(): string
    {
        return url("/page/{$this->slug}");
    }

}
