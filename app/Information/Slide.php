<?php

namespace App\Information;

use App\Model;
use App\Contracts\Sortable;
use App\Contracts\Activable;
use App\Contracts\Thumbnailable;

class Slide extends Model implements Thumbnailable
{

    use Sortable, Activable;

    /**
     * @var string
     */
    protected $table = 'slides';

    /**
     * @var array
     */
    protected $fillable = [
        'image',
        'sort',
        'is_active',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'is_active' => 'boolean',
    ];

    /**
     * @var bool
     */
    public $timestamps = false;


    /**
     * @return array
     */
    public static function rules(): array
    {
        return [
            'image' => 'required',
            'sort' => 'required|min:1',
        ];
    }


    /**
     * @return array
     */
    public static function thumbnailSizes(): array
    {
        return [200, 200];
    }

}
