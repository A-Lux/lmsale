<?php

namespace App\Providers;

use App\Settings;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * @return void
     */
    public function boot(): void
    {
        \App::singleton('Settings', function () {
            return new Settings();
        });

        // Validation
        \Validator::extend('password', function ($attribute, $value, $parameters, $validator) {
            return \Hash::check($value, auth()->user()->password);
        });

        // Auth blade directives
        \Blade::directive('auth', function () {
            return "<?php if (auth()->check()): ?>";
        });

        \Blade::directive('endauth', function () {
            return "<?php endif; ?>";
        });

        \Blade::directive('unauth', function () {
            return "<?php if (auth()->guest()): ?>";
        });

        \Blade::directive('endunauth', function () {
            return "<?php endif; ?>";
        });
    }


    /**
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton('product-storage', \App\Catalog\ProductStorage::class);
    }

}
