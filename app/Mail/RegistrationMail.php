<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;

class RegistrationMail extends Mailable
{

    /**
     * @var \App\Member\User
     */
    protected $user;

    /**
     * @var string
     */
    private $password;


    /**
     * @param \App\Member\User $user
     * @param string $password
     * @return void
     */
    public function __construct(\App\Member\User $user, string $password)
    {
        $this->user = $user;
        $this->password = $password;

        $this->subject('Регистрация на портале');
    }


    /**
     * @return RegistrationMail
     */
    public function build(): RegistrationMail
    {
        return $this->markdown('mail.registration', [
            'user' => $this->user,
            'password' => $this->password,
        ]);
    }

}
