<?php

namespace App;

use App\Catalog\Price;
use App\Catalog\Product;
use App\Catalog\Discount;
use App\Catalog\Category;
use App\Catalog\PriceType;
use App\Catalog\Shop\Order;

class WSDL
{

    /**
     * @var string
     */
    protected $url = 'http://88.204.145.238/liquimoly/ws/onlinestore.1cws?wsdl';

    /**
     * @var \SoapClient
     */
    protected $client;

    /**
     *
     * @var Singleton
     */
    private static $instance;


    public function __construct()
    {
        $this->client = new \SoapClient($this->url);
    }


    public static function init(): WSDL
    {
        if (is_null(self::$instance)) {
            self::$instance = new WSDL();
        }

        return self::$instance;
    }


    /**
     * @param bool $truncate
     *
     * @return bool
     */
    public function DiscountsApply($truncate = false): bool
    {
        if ($truncate == true) {
            Discount::query()->truncate();
        }

        $response = $this->client->DiscountsApply(['GUID' => '']);

        foreach ($response->return->ListDiscountsApply as $item) {
            if ($item->EndData == '0001-01-01') {
                $item->EndData = "3000-01-01";
            }

            Discount::create([
                'product_guid' => isset($item->NomenclatureGUID) ? $item->NomenclatureGUID : null,
                'user_guid' => isset($item->GUIDOfClient) ? $item->GUIDOfClient : null,
                'discount' => $item->discount,
                'order_sum' => isset($item->SumOrder) ? (int)$item->SumOrder : null,
                'started_at' => date('Y-m-d', strtotime($item->BeginData)),
                'finished_at' => date('Y-m-d', strtotime($item->EndData)),
            ]);
        }

        return true;
    }


    /**
     * @param bool $truncate
     *
     * @return bool
     */
    public function DownloadDataForSearchSystem($truncate = false): bool
    {
        $response = $this->client->DownloadDataForSearchSystem([]);
        $price_types = [];
        $categories = [];

        if (isset($response->return)) {
            if (isset($response->return->TypesOfPricesTable)) {
                $price_types = $response->return->TypesOfPricesTable;
            }

            if (isset($response->return->GoodsCategories)) {
                $categories = $response->return->GoodsCategories->CategoriesTable;
            }
        }

        if (count($price_types)) {
            if ($truncate === true) {
                \DB::table('price_types')->truncate();
            }

            foreach ($price_types as $data) {
                if ($truncate || ! $price_type = PriceType::where('guid', $data->GUID)->first()) {
                    $price_type = new PriceType;
                }

                $price_type->setRawAttributes([
                    'guid' => $data->GUID,
                    'name' => $data->Descriptions,
                ]);
                $price_type->save();
            }
        }

        if (count($categories)) {
            if ($truncate === true) {
                \DB::table('categories')->truncate();
            }

            foreach ($categories as $data) {
                if ($truncate || ! $category = Category::where('guid', $data->CategoryGUID)->first()) {
                    $category = new Category;
                }

                $category->setRawAttributes([
                    'guid' => $data->CategoryGUID,
                    'parent_guid' => ($data->ParantCategoryGUID) ? $data->ParantCategoryGUID : null,
                    'name' => $data->CategoryName,
                    'is_active' => ! $data->Hidden,
                    'sort' => $data->Sorting,
                ]);
                $category->save();
            }
        }

        return true;
    }


    /**
     * @param array $prices
     * @param array $counts
     * @param bool  $new
     * @param bool  $truncate
     *
     * @return bool
     * @internal param bool $update
     */
    public function DownloadNomenclaturesOnSite(
        array $prices,
        array $counts,
        bool $new = false,
        bool $truncate = false
    ): bool {
        $response = $this->client->DownloadNomenclaturesOnSite([
            'GategoryGUID' => null,
            'OnlyNew' => $new,
            'NumberOfItems' => 0,
        ]);

        $products = [];
        foreach ($response->return->NomenclatureDescriptionTable as $key => $product) {
            if ( ! $product->CategotyGUID || ! $product->Article) {
                continue;
            }

            $products[] = $product;
        }

        if (count($products)) {
            if ($truncate) {
                \DB::table('products')->truncate();
            }

            foreach ($products as $data) {
                if ($truncate || ! $product = Product::where('guid', $data->GUID)->first()) {
                    $product = new Product;
                }

                $product->setRawAttributes([
                    'name' => $data->Name,
                    'guid' => $data->GUID,
                    'code' => $data->Article,
                    'price' => isset($prices[$data->GUID][PriceType::DEFAULT_GUID]) ? $prices[$data->GUID][PriceType::DEFAULT_GUID] : 0,
                    'count_total' => isset($counts[$data->GUID]) ? $counts[$data->GUID] : 0,
                    'count_order' => $data->NotOrderMore,
                    'count_show' => $data->ShowNoMore,
                    'category_id' => Category::where('guid', $data->CategotyGUID)->first()->id,
                    'is_active' => ! $data->ShowNoMore,
                    'has_no_sale' => isset($data->NomenclaturePromotional) ? (bool)$data->NomenclaturePromotional : false,
                ]);

                $product->save();
            }
        }

        return true;
    }


    /**
     * @return array
     */
    public function DownloadPricesOfNomenclatures(): array
    {
        $response = $this->client->DownloadPricesOfNomenclatures([
            'NomenclatureListForPrices' => [],
        ]);

        $prices = [];

        \DB::table('prices')->truncate();

        foreach ($response->return->PricesOfNomenclatures as $item) {
            $prices[$item->NomenclatureGUID][$item->NomenclatureTypesOfPrices] = $item->NomenclaturePrice;

            Price::create([
                'product_guid' => $item->NomenclatureGUID,
                'type_guid' => $item->NomenclatureTypesOfPrices,
                'price' => $item->NomenclaturePrice,
            ]);
        }

        return $prices;
    }


    /**
     * @return array
     */
    public function DownloadCountOfNomenclatures(): array
    {
        $response = $this->client->DownloadCountOfNomenclatures([
            'NomenclatureListForCounts' => [],
        ]);

        $counts = [];

        foreach ($response->return->CountOfNomenlatures as $item) {
            $counts[$item->NomenclatureGUID] = $item->NomenclatureCount;
        }

        return $counts;
    }


    /**
     * @param string $guid
     *
     * @param bool   $isCreateDocument
     *
     * @return string
     */
    public function DownloadDocument(string $guid, bool $isCreateDocument): string
    {
        $response = $this->client->DownloadDocument([
            'GUID' => $guid,
            'CreateDocumet' => $isCreateDocument,
        ]);

        return base64_decode($response->return);
    }


    /**
     * @param Order $order
     *
     * @return string
     */
    public function CreateOrderOfBuyer(Order $order): string
    {
        $products = [];

        foreach ($order->items as $item) {
            $products[] = [
                'NomenclatureGUID' => $item->product->guid,
                'NomenclatureCount' => $item->count ?? 1,
                'NomenclaturePrice' => $item->price,
                'NomenclatureDiscount' => $item->discount,
            ];
        }

        $response = $this->client->CreateOrderOfBuyer([
            'CreationParametersOfOrder' => [
                'UserGUID' => $order->user->guid,
                'NumenclaturesOfOrder' => $products,
                'Сomment' => $order->delivery_comment,
            ],
        ]);

        return $response->return->OrderGUIDOrError;
    }

}
