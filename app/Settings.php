<?php

namespace App;

class Settings
{

    /**
     * @var array
     */
    protected $config = [];

    /**
     * @var string
     */
    protected $file = '.settings';


    /**
     * Settings constructor.
     */
    public function __construct()
    {
        $this->file = config_path($this->file);
        $this->config = $this->load();
    }


    /**
     * @param string $key
     * @return mixed
     */
    public function get(?string $key = null)
    {
        return $this->getConfigKey($key);
    }


    /**
     * @param string $key
     * @param string $value
     */
    public function set(string $key, string $value): void
    {
        $this->config[$key] = $value;
    }


    /**
     * @return array
     */
    public function load(): array
    {
        return json_decode(file_get_contents($this->file), true);
    }


    /**
     * @return int
     */
    public function save(): int
    {
        return file_put_contents($this->file, json_encode($this->config));
    }


    /**
     * @return array
     */
    public function getConfig(): array
    {
        return $this->config;
    }


    /**
     * @param array $config
     * @return Settings
     */
    public function setConfig(array $config): Settings
    {
        $this->config = $config;

        return $this;
    }


    /**
     * @param string $key
     * @param null|string $value
     * @return mixed
     */
    protected function getConfigKey(string $key, ?string $value = null)
    {
        $keys = explode('.', $key);

        if (count($keys) == 1) {
            if ( ! is_null($value)) {
                $this->config[$keys[0]] = $value;
            }

            return $this->config[$keys[0]];
        }

        if ( ! is_null($value)) {
            $this->config[$keys[0]][$keys[1]] = $value;
        }

        return $this->config[$keys[0]][$keys[1]];
    }

}
