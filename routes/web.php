<?php

Route::get('/', 'PageController@home');
Route::get('/page/{page}', 'PageController@card');

Route::get('/posts', 'PostController@catalog');
Route::get('/post/{post}', 'PostController@card');

Route::get('/api/register', 'ApiController@register');
Route::get('/api/recount', 'ApiController@recount');
Route::get('/api/reprice', 'ApiController@reprice');

Route::get('/products', 'ProductController@catalog');
Route::get('/products/search', 'ProductController@search');
Route::get('/products/{category}', 'ProductController@filtered');
Route::get('/product/{category}/{product}', 'ProductController@card');



Route::group([
    'prefix' => 'auth',
    'namespace' => 'Auth',
    'middleware' => 'guest',
], function (): void {
	Route::get('/test', 'SigninController@test');
    Route::get('/signin', 'SigninController@form');
    Route::post('/signin', 'SigninController@login');

    Route::group([
        'namespace' => 'Password',
        'prefix' => 'password',
    ],function (): void {
        Route::get('/restore', 'RestoreController@form');
        Route::post('/restore', 'RestoreController@sendResetLinkEmail');
        Route::get('/recovery/{token?}', 'RecoveryController@form')->name('password.reset');
        Route::post('/recovery', 'RecoveryController@reset');
    });
});

Route::group(['middleware' => 'signed'], function (): void {
    Route::get('/account', 'AccountController@profile');
    Route::get('/account/signout', 'AccountController@signout');

    Route::group([
        'namespace' => 'Settings',
        'prefix' => 'settings',
    ],function (): void {
        Route::get('/password', 'PasswordController@form');
        Route::post('/password', 'PasswordController@process');
    });

    Route::get('/basket', 'BasketController@table');
    Route::post('/basket/add', 'BasketController@store');
    Route::get('/basket/{basket}/delete', 'BasketController@remove');

    Route::group(['prefix' => 'ajax'], function(): void {
        Route::post('/basket/add', 'AjaxController@basket_add');
        Route::post('/basket/{basket}/recount', 'AjaxController@basket_recount');
        Route::post('/basket/{basket}remove', 'AjaxController@basket_remove');
    });

    Route::post('/preorder', 'OrderController@preorder');
    Route::get('/order', 'OrderController@form');
    Route::post('/order', 'OrderController@store');
    Route::get('/order/success', 'OrderController@success');
    Route::get('/order/{order}', 'OrderController@table');
    Route::get('/order/{order}/document', 'OrderController@document');
});

Route::group([
    'prefix' => 'backend',
    'namespace' => 'Backend',
    'middleware' => 'admin',
], function (): void {
    Route::get('/access', 'BackendController@access');

    Route::group(['middleware' => 'admin'], function (): void {
        // main
        Route::get('/', 'OrdersController@index');

        // upload
        Route::post('upload/file/{path}', 'UploadController@file');
        Route::post('upload/picture/{path}', 'UploadController@image');
        Route::post('upload/tinymce', 'UploadController@tinymce');

        // resources
        Route::resource('categories', 'CategoriesController', ['except' => ['show']]);
        Route::resource('products', 'ProductsController', ['except' => ['show']]);
        Route::resource('posts', 'PostsController', ['except' => ['show']]);
        Route::resource('slides', 'SlidesController', ['except' => ['show']]);
        Route::resource('groups', 'GroupsController', ['except' => ['show']]);
        Route::resource('users', 'UsersController', ['except' => ['show']]);

        // Custom
        Route::get('/settings', 'SettingsController@index');
        Route::post('/settings', 'SettingsController@store');

        Route::get('/orders', 'OrdersController@index');
        Route::get('/orders/{order}', 'OrdersController@show');

        Route::get('/sync', function(\Illuminate\Http\Request $request) {
            \Artisan::call($request->query('cmd'));

            return redirect()->back()->with('success', 'Команда успешно выполнена');
        });
    });

});
