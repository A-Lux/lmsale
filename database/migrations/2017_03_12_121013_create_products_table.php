<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{

    /**
     * @return void
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table): void {
            $table->increments('id');
            $table->string('guid')->unique();
            $table->string('code', 20)->unique()->index();
            $table->string('name')->unique();
            $table->integer('price');
            $table->string('image')->nullable();
            $table->text('description')->nullable();
            $table->longText('content')->nullable();
            $table->longText('about_properties')->nullable();
            $table->longText('about_application')->nullable();
            $table->longText('about_packing')->nullable();
            $table->longText('about_documentation')->nullable();
            $table->longText('about_analogs')->nullable();
            $table->integer('category_id')->unsigned();
            $table->boolean('is_line_top')->default(0);
            $table->boolean('is_line_profy')->default(0);
            $table->integer('sort')->default(0);
            $table->boolean('is_active')->default(0);
            $table->timestamps();
        });
    }


    /**
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }

}
