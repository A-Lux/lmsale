<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasketsTable extends Migration
{

    /**
     * @return void
     */
    public function up(): void
    {
        Schema::create('baskets', function (Blueprint $table): void {
            $table->increments('id');
            $table->integer('count');
            $table->integer('user_id')->unsigned();
            $table->integer('product_id')->unsigned();
        });
    }


    /**
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('baskets');
    }

}
