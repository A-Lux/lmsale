<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{

    /**
     * @return void
     */
    public function up(): void
    {
        Schema::create('companies', function (Blueprint $table): void {
            $table->increments('id');
            $table->string('title')->unique();
            $table->string('phone', 20);
            $table->string('address');
            $table->string('individual_identification_number', 11)->unique();
            $table->string('taxpayer_identification_number', 11)->unique();
            $table->string('bank_identification_code', 10)->unique();
            $table->integer('user_id')->unsigned();
            $table->integer('manager_id')->nullable();
        });
    }


    /**
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('companies');
    }

}
