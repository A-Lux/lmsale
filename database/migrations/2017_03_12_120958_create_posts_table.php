<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{

    /**
     * @return void
     */
    public function up(): void
    {
        Schema::create('posts', function (Blueprint $table): void {
            $table->increments('id');
            $table->string('guid')->unique();
            $table->string('name')->unique();
            $table->text('description');
            $table->longText('content');
            $table->string('image')->nullable();
            $table->integer('sort')->default(0);
            $table->integer('is_active')->default(0);
            $table->timestamps();
        });
    }


    /**
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('posts');
    }

}
