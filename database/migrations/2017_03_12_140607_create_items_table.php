<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{

    /**
     * @return void
     */
    public function up(): void
    {
        Schema::create('items', function (Blueprint $table): void {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->integer('count');
            $table->integer('price');
        });
    }


    /**
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('items');
    }

}
