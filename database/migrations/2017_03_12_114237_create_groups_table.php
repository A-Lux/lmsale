<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsTable extends Migration
{

    /**
     * @return void
     */
    public function up(): void
    {
        Schema::create('groups', function (Blueprint $table): void {
            $table->increments('id');
            $table->string('guid')->unique();
            $table->string('name')->unique();
        });
    }


    /**
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('groups');
    }

}
