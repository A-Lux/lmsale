<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{

    /**
     * @return void
     */
    public function up(): void
    {
        Schema::create('categories', function (Blueprint $table): void {
            $table->increments('id');
            $table->string('guid')->unique();
            $table->string('name')->unique();
            $table->string('parent_guid')->nullable();
            $table->integer('sort')->default(0);
            $table->boolean('is_active')->default(0);
        });
    }


    /**
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('categories');
    }

}
