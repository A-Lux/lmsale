<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{

    /**
     * @return void
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table): void {
            $table->increments('id');
            $table->string('guid')->unique();
            $table->integer('user_id')->unsigned();
            $table->string('delivery_name');
            $table->string('delivery_phone');
            $table->string('delivery_email');
            $table->string('delivery_comment')->nullable();
            $table->timestamps();
        });
    }


    /**
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }

}
