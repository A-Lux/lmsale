<?php

use Illuminate\Database\Seeder;

class DeliveriesTableSeeder extends Seeder
{

    /**
     * @var array
     */
    protected $data = [
        ['name' => 'Доставка курьером'],
        ['name' => 'Самовывоз'],
    ];


    /**
     * @return void
     */
    public function run(): void
    {
        \DB::table('deliveries')->truncate();

        foreach ($this->data as $data) {
            $data['guid'] = null;
            \App\Catalog\Shop\Delivery::create($data);
        }
    }

}
