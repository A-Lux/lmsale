<?php

use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{

    /**
     * @var array
     */
    protected $data = [
        [
            'name' => 'Администратор',
        ],
        [
            'name' => 'Менеджер',
        ],
        [
            'name' => 'Обычный клиент',
        ],
        [
            'name' => 'Частный клиент',
        ],
        [
            'name' => 'Постоянный клиент',
        ],
    ];


    /**
     * @return void
     */
    public function run(): void
    {
        \DB::table('groups')->truncate();

        foreach ($this->data as $data) {
            $data['guid'] = null;
            \App\Member\Group::create($data);
        }
    }

}
