<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{

    /**
     * @var array
     */
    protected $data = [
        ['name' => 'В обработке'],
        ['name' => 'Принят'],
        ['name' => 'Подготовлен'],
        ['name' => 'Выполнен'],
    ];


    /**
     * @return void
     */
    public function run(): void
    {
        \DB::table('statuses')->truncate();

        foreach ($this->data as $data) {
            $data['guid'] = null;
            \App\Catalog\Shop\Status::create($data);
        }
    }

}
