<?php

use Illuminate\Database\Seeder;

class SlidesTableSeeder extends Seeder
{
    /**
     * @return void
     */
    public function run()
    {
        \DB::table('slides')->truncate();

        for($i = 1; $i <= 3; $i++) {
            \App\Information\Slide::create([
                'sort' => $i,
                'is_active' => true,
                'image' => '/img/slide.png',
            ]);
        }
    }

}
