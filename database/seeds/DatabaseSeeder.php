<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    /**
     * @return void
     */
    public function run(): void
    {
        $this->call(DeliveriesTableSeeder::class);
        $this->call(StatusesTableSeeder::class);
        $this->call(GroupsTableSeeder::class);
        $this->call(PagesTableSeeder::class);
        $this->call(SlidesTableSeeder::class);
    }

}
