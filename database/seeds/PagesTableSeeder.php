<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{

    /**
     * @var array
     */
    protected $data = [
        [
            'name' => 'Контакты',
            'slug' => 'contacts',
            'content' => null,
            'is_active' => 1,
        ],
    ];


    /**
     * @return void
     */
    public function run()
    {
        \DB::table('pages')->truncate();

        foreach ($this->data as $data) {
            \App\Information\Page::create($data);
        }
    }

}
