<div class="cart-table-row">
    <div class="cart-table-cell cart-table-cell-name cart-table-cont">
        <div class="cart-table-img">
            <a href="{{ $basket->product->link() }}"><img src="{{ $basket->product->image }}"></a>
        </div>

        <div class="cart-table-desc">
            <div class="cart-table-title">
                <a href="{{ $basket->product->link() }}">{{ $basket->product->name }}</a>
            </div>

            <div class="cart-table-article">
                Артикул: {{ $basket->product->code }}
            </div>
        </div>
    </div>

    <div class="cart-table-cell cart-table-cell-price">
        {{ $basket->count }}
    </div>

    <div class="cart-table-cell cart-table-cell-price">
        {{ number_format($basket->product->price_discount, 0, '.', ' ') }} ₸
    </div>
    <div class="cart-table-cell cart-table-cell-summ">
        {{ $basket->amount() }} ₸
    </div>
</div>

