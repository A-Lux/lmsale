@extends('layouts.app', [
    'breadcrumbs' => [
        '/' => 'Главная',
        '/account' => 'Личный кабинет',
        $order->link() => "Заказ #{$order->id}",
    ],
])

@section('title', "Заказ #{$order->id}")

@section('content')
    <section class="section-main">
        <div class="container">
            @include('layouts.partials.aside')

            <section class="content">
                <div class="cart-page">
                    <div class="cart-info">
                        <div class="cart-page-title">
                            <span></span>{{ "Заказ #{$order->id}" }}
                        </div>

                        <div class="cart-info-details">
                            <div class="cart-info-quantity cart-info-item">
                                Количество товаров: <span>{{ number_format(count($order->items), 0, '.', ' ') }}</span>
                            </div>

                            <div class="cart-info-summ cart-info-item">
                                На общую сумму: <span>{{ $order->amount() }} ₸</span>
                            </div>
                        </div>
                    </div>

                    <div class="cart-table">
                        <div class="cart-table-header cart-table-row">
                            <div class="cart-table-cell cart-table-cell-name">Наименование</div>
                            <div class="cart-table-cell cart-table-cell-quantity">Кол-во</div>
                            <div class="cart-table-cell cart-table-cell-price">Цена</div>
                            <div class="cart-table-cell cart-table-cell-summ">Сумма</div>
                        </div>

                        @each('order.table-item', $baskets, 'basket')

                        <div class="cart-table-row cart-table-footer">
                            <div class="cart-table-cell cart-table-cell-name"></div>
                            <div class="cart-table-cell cart-table-cell-quantity"></div>
                            <div class="cart-table-cell cart-table-cell-price"></div>
                            <div class="cart-table-cell cart-table-cell-summ">
                                    Итог: <br/>
                                    <span>{{ $order->amount() }}</span> ₸
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection