@extends('layouts.app', [
    'breadcrumbs' => [
        '/' => 'Главная',
        '/basket' => 'Корзина',
        '/order' => 'Оформление заказа',
    ],
])

@section('title', 'Оформление заказа')

@section('content')
    <section class="section-main">
        <div class="container">
            @include('layouts.partials.aside')

            <section class="content">
                <div class="delivery-page">
                    <div class="cart-steps">
                        <div class="cart-steps-item">1. Ваша корзина</div>
                        <div class="cart-steps-item is-active">2. Доставка и оплата</div>
                    </div>

                    @if (count($errors->all()))
                        <div class="errors">
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </div>
                    @endif

                    <div class="delivery-form-wrap">
                        <form class="delivery-form" action="{{ url('/order') }}" method="post">
                            @if (auth()->user()->group_id == 2)
                                <div class="delivery-type">
                                    <div class="delivery-type-label">Выберите контрагента:</div>

                                    <div class="delivery-type-select">
                                        <div class="delivery-type-selected">
                                            &nbsp;
                                        </div>

                                        <div class="delivery-type-dropdown"></div>
                                        <div class="delivery-type-options">
                                            @foreach(auth()->user()->contragents() as $user)
                                                <div data-id="{{ $user->id }}" class="delivery-type-options-item">
                                                    <label>{{ $user->name }}</label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" name="user_id"/>
                            @else
                                <div class="delivery-inputs">
                                    <input class="delivery-inputs-item" name="delivery_name" type="text"
                                           value="{{ old('delivery_name', $user->name) }}"
                                           placeholder="Ваше имя"/>

                                    <input class="delivery-inputs-item" name="delivery_phone" type="text"
                                           value="{{ old('delivery_phone') }}"
                                           placeholder="Контактный телефон"/>

                                    <input class="delivery-inputs-item" name="delivery_email" type="text"
                                           value="{{ old('delivery_email', $user->email) }}" placeholder="E-mail"/>

                                    <textarea class="delivery-inputs-item delivery-inputs-item-textarea"
                                              name="delivery_comment"
                                              placeholder="Комментарий к заказу">{{ old('delivery_comment') }}</textarea>
                                </div>
                            @endif

                            {{ csrf_field() }}

                            <div class="cart-confirm">
                                <button class="btn-confirm btn-cart">Заказать</button>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection