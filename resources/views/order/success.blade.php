@extends('layouts.app', [
    'breadcrumbs' => [
        '/' => 'Главная',
        '/basket' => 'Корзина',
        '/order/success' => 'Заказ оформлен',
    ],
])

@section('title', 'Заказ оформлен')

@section('content')
    <section class="section-main">
        <div class="container">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="auth success-page">
                    <div class="success-block">
                        <div class="success-title">Спасибо за ваш заказ!</div>
                        <div class="success-message">Статус вашего заказа можно узнать по номеру:</div>
                        <div class="success-cont">
                            @foreach(explode(',', app('Settings')->get('contacts.phones')) as $phone)
                                <a href="tel:{{ $phone }}">{{ $phone }}</a>
                            @endforeach
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection