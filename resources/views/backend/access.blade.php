<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Icon -->
    <link rel="icon" type="image/png" href="/favicon.ico"/>

    <title>Ошибка доступа — {{ config('app.name') }}</title>

    {{-- Styles --}}
    <link href="{{ mix('/css/backend/app.css') }}" rel="stylesheet">
</head>
<body>

<section class="hero is-danger is-bold is-fullheight">
    <div class="hero-body">
        <div class="container">
            <h1 class="title" align="center">
                Отказано в доступе
            </h1>
        </div>
    </div>
</section>

</body>
</html>
