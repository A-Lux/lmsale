{{ csrf_field() }}

<div class="control is-horizontal">
    <div class="control-label">
        <label class="label">Название</label>
    </div>
    <div class="control">
        <p class="control is-width-full">
            @include('backend.layouts.includes.form.field.input', ['attribute' => 'name'])
        </p>
    </div>
</div>

<div class="control is-horizontal">
    <div class="control-label">
        <label class="label">Категория</label>
    </div>
    <div class="control">
        <p class="control is-width-full">
            @include('backend.layouts.includes.form.field.dropdown', [
                'attribute' => 'category_id',
                'options' => $dependencies['category'],
            ])
        </p>
    </div>
</div>

<hr />

<div class="control is-horizontal">
    <div class="control-label">
        <label class="label">Артикул</label>
    </div>
    <div class="control">
        <p class="control is-width-one-quarter">
            @include('backend.layouts.includes.form.field.input', ['attribute' => 'code'])
        </p>
    </div>
</div>

<div class="control is-horizontal">
    <div class="control-label">
        <label class="label">Цена</label>
    </div>
    <div class="control">
        <p class="control is-width-one-quarter">
            @include('backend.layouts.includes.form.field.input', ['attribute' => 'price', 'type' => 'number'])
        </p>
    </div>
</div>

<hr />

<div class="control is-horizontal">
    <div class="control-label">
        <label class="label">Описание</label>
    </div>
    <div class="control">
        <p class="control is-width-full">
            @include('backend.layouts.includes.form.field.tinymce', ['attribute' => 'description'])
        </p>
    </div>
</div>

<div class="control is-horizontal">
    <div class="control-label">
        <label class="label">Контент</label>
    </div>
    <div class="control">
        <p class="control is-width-full">
            @include('backend.layouts.includes.form.field.tinymce', ['attribute' => 'content'])
        </p>
    </div>
</div>

<hr />

<div class="control is-horizontal">
    <div class="control-label">
        <label class="label">Свойства</label>
    </div>
    <div class="control">
        <p class="control is-width-full">
            @include('backend.layouts.includes.form.field.tinymce', ['attribute' => 'about_properties'])
        </p>
    </div>
</div>

<div class="control is-horizontal">
    <div class="control-label">
        <label class="label">Применение</label>
    </div>
    <div class="control">
        <p class="control is-width-full">
            @include('backend.layouts.includes.form.field.tinymce', ['attribute' => 'about_application'])
        </p>
    </div>
</div>

<div class="control is-horizontal">
    <div class="control-label">
        <label class="label">Фасовка</label>
    </div>
    <div class="control">
        <p class="control is-width-full">
            @include('backend.layouts.includes.form.field.tinymce', ['attribute' => 'about_packing'])
        </p>
    </div>
</div>

<div class="control is-horizontal">
    <div class="control-label">
        <label class="label">Документация</label>
    </div>
    <div class="control">
        <p class="control is-width-full">
            @include('backend.layouts.includes.form.field.tinymce', ['attribute' => 'about_documentation'])
        </p>
    </div>
</div>

<div class="control is-horizontal">
    <div class="control-label">
        <label class="label">Аналоги</label>
    </div>
    <div class="control">
        <p class="control is-width-full">
            @include('backend.layouts.includes.form.field.tinymce', ['attribute' => 'about_analogs'])
        </p>
    </div>
</div>

<hr />

<div class="control is-horizontal">
    <div class="control-label">
        <label class="label">Изображение</label>
    </div>
    <div class="control">
        <p class="control is-width-full">
            @include('backend.layouts.includes.form.field.image', [
                'attribute' => 'image',
                'path' => 'products',
            ])
        </p>
    </div>
</div>

<hr />

<div class="control is-horizontal">
    <div class="control-label">
        <label class="label"></label>
    </div>
    <div class="control">
        <p class="control is-width-full">
            @include('backend.layouts.includes.form.field.checkbox', [
                'attribute' => 'is_line_top',
                'label' => 'Линейка "TOP"',
            ])
        </p>
    </div>
</div>

<div class="control is-horizontal">
    <div class="control-label">
        <label class="label"></label>
    </div>
    <div class="control">
        <p class="control is-width-full">
            @include('backend.layouts.includes.form.field.checkbox', [
                'attribute' => 'is_line_top',
                'label' => 'Линейка "Profy"',
            ])
        </p>
    </div>
</div>

<hr />

<div class="control is-horizontal">
    <div class="control-label">
        <label class="label">Сортировка</label>
    </div>
    <div class="control">
        <p class="control is-width-one-quarter">
            @include('backend.layouts.includes.form.field.input', [
                'attribute' => 'sort',
                'type' => 'number',
            ])
        </p>
    </div>
</div>

<div class="control is-horizontal">
    <div class="control-label">
        <label class="label"></label>
    </div>
    <div class="control">
        <p class="control is-width-full">
            @include('backend.layouts.includes.form.field.checkbox', [
                'attribute' => 'is_active',
                'label' => 'Активность',
            ])
        </p>
    </div>
</div>

@include('backend.layouts.includes.script.onleave')
@include('backend.layouts.includes.script.tinymce')
