@extends('backend.layouts.app', ['active' => 'products'])

@section('section', 'Товары')
@section('action', 'Редактирование')

@section('content')
    @include('backend.layouts.includes.form.tinymce-upload')

    <form method="post" action="{{ route('products.update', ['id' => $entity->id]) }}">
        {{ method_field('PATCH') }}

        @include('backend.products.partials.form')

        @include('backend.layouts.includes.form.actions', ['action' => 'Сохранить изменения'])
    </form>
@endsection
