@extends('backend.layouts.app', ['active' => 'products'])

@section('section', 'Товары')
@section('action', 'Список')

@section('actions')
    {{--<a href="{{ route('products.create') }}" class="button is-dark is-outlined">Добавить запись</a>--}}
@endsection

@section('content')
    @include('backend.products.partials.filter')

    @if(count($entities))
        <table class="table is-striped is-narrow">
            <thead>
            <tr>
                <th width="50">ID</th>
                <th width="340">GUID</th>
                <th>Название</th>
                <th>Категория</th>
                <th>Активация</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($entities as $entity)
                <tr>
                    <td>{{ $entity->id }}</td>
                    <td>{{ $entity->guid }}</td>
                    <td>{{ $entity->name }}</td>
                    <td>{{ $entity->category->name }}</td>
                    <td>{{ $entity->is_active ? 'Да' : 'Нет' }}</td>
                    <td class="is-icon">
                        @include('backend.layouts.includes.index.table-actions', [
                            'entity' => 'products',
                            'delete' => false,
                            'id' => $entity->id,
                        ])
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $entities->appends(paginateAppends())->links() }}
    @else
        @include('backend.layouts.includes.index.no-results')
    @endif
@endsection