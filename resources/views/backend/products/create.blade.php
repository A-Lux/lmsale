@extends('backend.layouts.app', ['active' => 'products'])

@section('section', 'Товары')
@section('action', 'Создание')

@section('content')
    @include('backend.layouts.includes.form.tinymce-upload')

    <form method="post" action="{{ route('products.store') }}">
        @include('backend.products.partials.form')

        @include('backend.layouts.includes.form.actions', ['action' => 'Добавить запись'])
    </form>
@endsection
