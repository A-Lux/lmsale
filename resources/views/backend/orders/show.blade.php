@extends('backend.layouts.app', ['active' => 'orders'])

@section('section', 'Заказы')
@section('action', "№{$entity->id}")

@section('content')
    <h2>Данные заказа</h2>

    <table class="table is-bordered">
        <thed>
            <tr>
                <th>Продукт</th>
                <th>Кол-во / цена</th>
                <th>Сумма</th>
            </tr>
        </thed>
        <tbody>
        @foreach($entity->items as $item)
            <tr>
                <td>
                    <a target="_blank" href="{{ $item->product->link() }}">{{ $item->product->name }}</a>
                </td>
                <td>
                    {{ $item->count }} шт. / {{ $item->amount() }} ₸
                </td>
                <td>
                    {{ $item->amount() }} ₸
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <h2>Данные доставки</h2>

    <table class="table is-bordered">
        <tbody>
        <tr>
            <th>ФИО</th>
            <td>{{ $entity->delivery_name }}</td>
        </tr>
        <tr>
            <th>Телефон</th>
            <td>{{ $entity->delivery_phone }}</td>
        </tr>
        <tr>
            <th>Email</th>
            <td>{{ $entity->delivery_email }}</td>
        </tr>
        @if ($entity->delivery_comment)
            <tr>
                <th>Комментарий</th>
                <td>{{ $entity->delivery_comment }}</td>
            </tr>
        @endif
        </tbody>
    </table>

    <h2>Данные клиента</h2>

    <table class="table is-bordered">
        <tbody>
        <tr>
            <th width="200">Название</th>
            <td>{{ $entity->user->name }}</td>
        </tr>
        <tr>
            <th>ИИН/БИН</th>
            <td>{{ $entity->user->iin }}</td>
        </tr>
        </tbody>
    </table>
@endsection