@extends('backend.layouts.app', ['active' => 'orders'])

@section('section', 'Заказы')
@section('action', 'Список')

@section('content')
    @include('backend.orders.partials.filter')

    @if(count($entities))
        <table class="table is-striped is-narrow">
            <thead>
            <tr>
                <th width="50">ID</th>
                <th width="340">GUID</th>
                <td>Клиент</th>
                <th>Кол-во / Сумма</th>
            </tr>
            </thead>
            <tbody>
            @foreach($entities as $entity)
                <tr>
                    <td>{{ $entity->id }}</td>
                    <td>{{ $entity->guid }}</td>
                    <td>
                        <a target="_blank" href="{{ url("/backend/orders/{$entity->id}") }}">
                            {{ $entity->user->name}}
                        </a>
                    </td>
                    <td>
                        {{ $entity->items()->count() }} шт. /
                        {{ $entity->amount() }} ₸
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $entities->appends(paginateAppends())->links() }}
    @else
        @include('backend.layouts.includes.index.no-results')
    @endif
@endsection