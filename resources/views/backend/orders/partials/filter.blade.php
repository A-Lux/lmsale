<div class="notification">
    <form method="GET" action="{{ url('/backend/orders') }}">
        <div class="control is-horizontal">
            <div class="control-label">
                <label class="label">ID</label>
            </div>
            <div class="control">
                <p class="control is-width-one-quarter">
                    @include('backend.layouts.includes.form.filter.input', ['attribute' => 'id'])
                </p>
            </div>
        </div>

        <div class="control is-horizontal">
            <div class="control-label">
                <label class="label">Заказчик</label>
            </div>
            <div class="control">
                <p class="control is-width-full">
                    @include('backend.layouts.includes.form.filter.dropdown', [
                        'attribute' => 'user',
                        'options' => \App\Member\User::orderBy('name')->get(),
                        'display' => 'name',
                    ])
                </p>
            </div>
        </div>

        <div class="control is-horizontal">
            <div class="control-label">
                <label class="label"></label>
            </div>
            <p class="control">
                <button type="submit" class="button is-info">Найти</button>
                <a href="{{ url('/backend/orders') }}" class="button is-link">Отменить</a>
            </p>
        </div>
    </form>
</div>
