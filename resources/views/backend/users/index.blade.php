@extends('backend.layouts.app', ['active' => 'users'])

@section('section', 'Пользователи')
@section('action', 'Список')

@section('actions')
    {{--<a href="{{ route('users.create') }}" class="button is-dark is-outlined">Добавить запись</a>--}}
@endsection

@section('content')
    @include('backend.users.partials.filter')

    @if(count($entities))
        <table class="table is-striped is-narrow">
            <thead>
            <tr>
                <th width="50">ID</th>
                <th width="340">GUID</th>
                <th>ФИО</th>
                <th>Группа</th>
                <th>Активация</th>
                <th width="60"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($entities as $entity)
                <tr>
                    <td>{{ $entity->id }}</td>
                    <td>{{ $entity->guid }}</td>
                    <td>{{ $entity->name }}</td>
                    <td>{{ $entity->group->name }}</td>
                    <td>{{ $entity->is_active ? 'Да' : 'Нет' }}</td>
                    <td class="is-icon">
                        @include('backend.layouts.includes.index.table-actions', [
                            'entity' => 'users',
                            'delete' => true,
                            'id' => $entity->id,
                        ])
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $entities->appends(paginateAppends())->links() }}
    @else
        @include('backend.layouts.includes.index.no-results')
    @endif
@endsection
