{{ csrf_field() }}

<div class="control is-horizontal">
    <div class="control-label">
        <label class="label">ФИО</label>
    </div>
    <div class="control">
        <p class="control is-width-full">
            @include('backend.layouts.includes.form.field.input', ['attribute' => 'name'])
        </p>
    </div>
</div>

<div class="control is-horizontal">
    <div class="control-label">
        <label class="label">Email</label>
    </div>
    <div class="control">
        <p class="control is-width-full">
            @include('backend.layouts.includes.form.field.input', ['attribute' => 'email', 'type' => 'email'])
        </p>
    </div>
</div>

<div class="control is-horizontal">
    <div class="control-label">
        <label class="label">ИИН/БИН</label>
    </div>
    <div class="control">
        <p class="control is-width-full">
            @include('backend.layouts.includes.form.field.input', ['attribute' => 'iin'])
        </p>
    </div>
</div>

<hr />

<div class="control is-horizontal">
    <div class="control-label">
        <label class="label">Пароль</label>
    </div>
    <div class="control">
        <p class="control is-width-full">
            @include('backend.layouts.includes.form.field.input', ['attribute' => 'new_password', 'type' => 'password'])
        </p>
    </div>
</div>

<hr />

<div class="control is-horizontal">
    <div class="control-label">
        <label class="label">Группа</label>
    </div>
    <div class="control">
        <p class="control is-width-full">
            @include('backend.layouts.includes.form.field.dropdown', [
                'attribute' => 'group_id',
                'options' => $dependencies['group'],
            ])
        </p>
    </div>
</div>

<div class="control is-horizontal">
    <div class="control-label">
        <label class="label">Менеджер</label>
    </div>
    <div class="control">
        <p class="control is-width-full">
            @include('backend.layouts.includes.form.field.dropdown', [
                'attribute' => 'manager_id',
                'options' => $dependencies['manager'],
            ])
        </p>
    </div>
</div>

<div class="control is-horizontal">
    <div class="control-label">
        <label class="label"></label>
    </div>
    <div class="control">
        <p class="control is-width-full">
            @include('backend.layouts.includes.form.field.checkbox', [
                'attribute' => 'is_active',
                'label' => 'Активность',
            ])
        </p>
    </div>
</div>

@include('backend.layouts.includes.script.onleave')
