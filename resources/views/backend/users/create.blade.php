@extends('backend.layouts.app', ['active' => 'users'])

@section('section', 'Пользователи')
@section('action', 'Создание')

@section('content')
    <form method="post" action="{{ route('users.store') }}">
        @include('backend.users.partials.form')

        @include('backend.layouts.includes.form.actions', ['action' => 'Добавить запись'])
    </form>
@endsection
