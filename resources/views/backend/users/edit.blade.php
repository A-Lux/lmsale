@extends('backend.layouts.app', ['active' => 'users'])

@section('section', 'Пользователи')
@section('action', 'Редактирование')

@section('content')
    <form method="post" action="{{ route('users.update', ['id' => $entity->id]) }}">
        {{ method_field('PATCH') }}

        @include('backend.users.partials.form')

        @include('backend.layouts.includes.form.actions', ['action' => 'Сохранить изменения'])
    </form>
@endsection
