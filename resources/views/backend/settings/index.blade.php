@extends('backend.layouts.app', ['active' => 'settings'])

@section('section', 'Настройки')
@section('action', 'Обновление')

@section('content')
    <form method="post" action="{{ url('/backend/settings') }}">
        {{ csrf_field() }}

        <h3>Социальные сети</h3>

        @foreach($entity['social'] as $key => $value)
            <div class="control is-horizontal">
                <div class="control-label">
                    <label class="label">{{ $key }}</label>
                </div>
                <div class="control">
                    <p class="control is-width-full">
                        <input class="input" type="text"  name="settings[social][{{ $key }}]" value="{{ $value }}" required />
                    </p>
                </div>
            </div>
        @endforeach

        <h3>Контакты</h3>

        @foreach($entity['contacts'] as $key => $value)
            <div class="control is-horizontal">
                <div class="control-label">
                    <label class="label">{{ $key }}</label>
                </div>
                <div class="control">
                    <p class="control is-width-full">
                        <input class="input" type="text"  name="settings[contacts][{{ $key }}]" value="{{ $value }}" required />
                    </p>
                </div>
            </div>
        @endforeach

        <h3>Прочее</h3>

        @foreach($entity['other'] as $key => $value)
            <div class="control is-horizontal">
                <div class="control-label">
                    <label class="label">{{ $key }}</label>
                </div>
                <div class="control">
                    <p class="control is-width-full">
                        <input class="input" type="text"  name="settings[other][{{ $key }}]" value="{{ $value }}" required />
                    </p>
                </div>
            </div>
        @endforeach

        @include('backend.layouts.includes.form.actions', ['action' => 'Сохранить изменения'])
    </form>
@endsection
