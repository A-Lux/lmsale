@extends('backend.layouts.app', ['active' => 'slides'])

@section('section', 'Слайды')
@section('action', 'Редактирование')

@section('content')
    <form method="post" action="{{ route('slides.update', ['id' => $entity->id]) }}">
        {{ method_field('PATCH') }}

        @include('backend.slides.partials.form')

        @include('backend.layouts.includes.form.actions', ['action' => 'Сохранить изменения'])
    </form>
@endsection
