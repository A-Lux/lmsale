@extends('backend.layouts.app', ['active' => 'slides'])

@section('section', 'Слайды')
@section('action', 'Создание')

@section('content')
    <form method="post" action="{{ route('slides.store') }}">
        @include('backend.slides.partials.form')

        @include('backend.layouts.includes.form.actions', ['action' => 'Добавить запись'])
    </form>
@endsection
