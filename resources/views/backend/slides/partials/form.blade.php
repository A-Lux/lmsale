{{ csrf_field() }}

<div class="control is-horizontal">
    <div class="control-label">
        <label class="label">Изображение</label>
    </div>
    <div class="control">
        <p class="control is-width-full">
            @include('backend.layouts.includes.form.field.image', [
                'attribute' => 'image',
                'path' => 'slides',
            ])
        </p>
    </div>
</div>

<hr />

<div class="control is-horizontal">
    <div class="control-label">
        <label class="label">Сортировка</label>
    </div>
    <div class="control">
        <p class="control is-width-one-quarter">
            @include('backend.layouts.includes.form.field.input', [
                'attribute' => 'sort',
                'type' => 'number',
            ])
        </p>
    </div>
</div>

<div class="control is-horizontal">
    <div class="control-label">
        <label class="label"></label>
    </div>
    <div class="control">
        <p class="control is-width-full">
            @include('backend.layouts.includes.form.field.checkbox', [
                'attribute' => 'is_active',
                'label' => 'Активность',
            ])
        </p>
    </div>
</div>


@include('backend.layouts.includes.script.onleave')
