@extends('backend.layouts.app', ['active' => 'slides'])

@section('section', 'Слайды')
@section('action', 'Список')

@section('actions')
    <a href="{{ route('slides.create') }}" class="button is-dark is-outlined">Добавить запись</a>
@endsection

@section('content')
    @include('backend.slides.partials.filter')

    @if(count($entities))
        <table class="table is-striped is-narrow">
            <thead>
            <tr>
                <th width="50">ID</th>
                <th>Изображение</th>
                <th>Активация</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($entities as $entity)
                <tr>
                    <td>{{ $entity->id }}</td>
                    <td>
                        <a href="{{ $entity->image }}" target="_blank">
                            <img src="{{ $entity->image }}" width="200">
                        </a>
                    </td>
                    <td>{{ $entity->is_active ? 'Да' : 'Нет' }}</td>
                    <td class="is-icon">
                        @include('backend.layouts.includes.index.table-actions', [
                            'entity' => 'slides',
                            'id' => $entity->id,
                        ])
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $entities->appends(paginateAppends())->links() }}
    @else
        @include('backend.layouts.includes.index.no-results')
    @endif
@endsection