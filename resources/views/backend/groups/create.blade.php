@extends('backend.layouts.app', ['active' => 'groups'])

@section('section', 'Группы пользователей')
@section('action', 'Создание')

@section('content')
    <form method="post" action="{{ route('groups.store') }}">
        @include('backend.groups.partials.form')

        @include('backend.layouts.includes.form.actions', ['action' => 'Добавить запись'])
    </form>
@endsection
