@extends('backend.layouts.app', ['active' => 'groups'])

@section('section', 'Группы пользователей')
@section('action', 'Список')

@section('actions')
{{--    <a href="{{ route('groups.create') }}" class="button is-dark is-outlined">Добавить запись</a>--}}
@endsection

@section('content')
    @include('backend.groups.partials.filter')

    @if(count($entities))
        <table class="table is-striped is-narrow">
            <thead>
            <tr>
                <th width="50">ID</th>
                <th>Название</th>
                {{--<th></th>--}}
            </tr>
            </thead>
            <tbody>
            @foreach($entities as $entity)
                <tr>
                    <td>{{ $entity->id }}</td>
                    <td>{{ $entity->name }}</td>
                    {{--<td class="is-icon">--}}
                        {{--@include('backend.layouts.includes.index.table-actions', [--}}
                            {{--'entity' => 'groups',--}}
                            {{--'id' => $entity->id,--}}
                        {{--])--}}
                    {{--</td>--}}
                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $entities->appends(paginateAppends())->links() }}
    @else
        @include('backend.layouts.includes.index.no-results')
    @endif
@endsection