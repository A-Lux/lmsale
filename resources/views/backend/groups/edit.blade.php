@extends('backend.layouts.app', ['active' => 'groups'])

@section('section', 'Группы пользователей')
@section('action', 'Редактирование')

@section('content')
    <form method="post" action="{{ route('groups.update', ['id' => $entity->id]) }}">
        {{ method_field('PATCH') }}

        @include('backend.groups.partials.form')

        @include('backend.layouts.includes.form.actions', ['action' => 'Сохранить изменения'])
    </form>
@endsection
