{{ csrf_field() }}

<div class="control is-horizontal">
    <div class="control-label">
        <label class="label">Название</label>
    </div>
    <div class="control">
        <p class="control is-width-full">
            @include('backend.layouts.includes.form.field.input', ['attribute' => 'name'])
        </p>
    </div>
</div>

@include('backend.layouts.includes.script.onleave')
