<div class="notification">
    <form method="GET" action="{{ url('/backend/categories') }}">
        <div class="control is-horizontal">
            <div class="control-label">
                <label class="label">ID</label>
            </div>
            <div class="control">
                <p class="control is-width-one-quarter">
                    @include('backend.layouts.includes.form.filter.input', ['attribute' => 'id'])
                </p>
            </div>
        </div>

        <div class="control is-horizontal">
            <div class="control-label">
                <label class="label">Название</label>
            </div>
            <div class="control">
                <p class="control is-width-full">
                    @include('backend.layouts.includes.form.filter.input', ['attribute' => 'name'])
                </p>
            </div>
        </div>

        <div class="control is-horizontal">
            <div class="control-label">
                <label class="label"></label>
            </div>
            <div class="control">
                <p class="control is-width-full">
                    @include('backend.layouts.includes.form.filter.checkbox', [
                        'attribute' => 'active',
                        'label' => 'Активация',
                    ])
                </p>
            </div>
        </div>

        <div class="control is-horizontal">
            <div class="control-label">
                <label class="label"></label>
            </div>
            <p class="control">
                <button type="submit" class="button is-info">Найти</button>
                <a href="{{ url('/backend/categories') }}" class="button is-link">Отменить</a>
            </p>
        </div>
    </form>
</div>
