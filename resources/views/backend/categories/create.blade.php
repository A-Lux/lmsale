@extends('backend.layouts.app', ['active' => 'categories'])

@section('section', 'Категории')
@section('action', 'Создание')

@section('content')
    <form method="post" action="{{ route('categories.store') }}">
        @include('backend.categories.partials.form')

        @include('backend.layouts.includes.form.actions', ['action' => 'Добавить запись'])
    </form>
@endsection
