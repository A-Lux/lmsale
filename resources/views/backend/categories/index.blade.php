@extends('backend.layouts.app', ['active' => 'categories'])

@section('section', 'Категории')
@section('action', 'Список')

@section('actions')
{{--    <a href="{{ route('categories.create') }}" class="button is-dark is-outlined">Добавить запись</a>--}}
@endsection

@section('content')
    @include('backend.categories.partials.filter')

    @if(count($entities))
        <table class="table is-striped is-narrow">
            <thead>
            <tr>
                <th width="50">ID</th>
                <th width="340">GUID</th>
                <th>Название</th>
                <th>Активация</th>
                {{--<th></th>--}}
            </tr>
            </thead>
            <tbody>
            @foreach($entities as $entity)
                <tr class="is-success">
                    <td>{{ $entity->id }}</td>
                    <td>{{ $entity->guid }}</td>
                    <td>
                        @if ($entity->parent())
                            {{ $entity->parent()->name }}
                        @else
                            <b>{{ $entity->name }}</b>
                        @endif
                    </td>
                    <td>{{ $entity->is_active ? 'Да' : 'Нет' }}</td>
                    {{--<td class="is-icon">--}}
                        {{--@include('backend.layouts.includes.index.table-actions', [--}}
                            {{--'entity' => 'categories',--}}
                            {{--'id' => $entity->id,--}}
                        {{--])--}}
                    {{--</td>--}}
                </tr>

                @if (count($entity->childs()))
                    @foreach($entity->childs() as $category)
                        <tr>
                            <td>{{ $category->id }}</td>
                            <td>{{ $category->guid }}</td>
                            <td class="is-icon">
                                —
                                <i>{{ $category->name }}</i>
                            </td>
                            <td>{{ $entity->is_active ? 'Да' : 'Нет' }}</td>
                            {{--<td class="is-icon">--}}
                                {{--@include('backend.layouts.includes.index.table-actions', [--}}
                                    {{--'entity' => 'categories',--}}
                                    {{--'id' => $category->id,--}}
                                {{--])--}}
                            {{--</td>--}}
                        </tr>
                    @endforeach
                @endif
            @endforeach
            </tbody>
        </table>

        {{ $entities->appends(paginateAppends())->links() }}
    @else
        @include('backend.layouts.includes.index.no-results')
    @endif
@endsection