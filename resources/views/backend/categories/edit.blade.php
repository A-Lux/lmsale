@extends('backend.layouts.app', ['active' => 'categories'])

@section('section', 'Категории')
@section('action', 'Редактирование')

@section('content')
    <form method="post" action="{{ route('categories.update', ['id' => $entity->id]) }}">
        {{ method_field('PATCH') }}

        @include('backend.categories.partials.form')

        @include('backend.layouts.includes.form.actions', ['action' => 'Сохранить изменения'])
    </form>
@endsection
