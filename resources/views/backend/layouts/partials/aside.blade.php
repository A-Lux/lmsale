<div class="column is-one-quarter">
    <aside class="menu">
        <p class="menu-label">
            Продукция
        </p>
        <ul class="menu-list">
            <li><a {{ is_active($active == 'categories') }} href="{{ url('/backend/categories') }}">Категории</a></li>
            <li><a {{ is_active($active == 'products') }} href="{{ url('/backend/products') }}">Товары</a></li>
            <li><a {{ is_active($active == 'orders') }} href="{{ url('/backend/orders') }}">Заказы</a></li>
        </ul>

        <p class="menu-label">
            Контент
        </p>
        <ul class="menu-list">
            <li><a {{ is_active($active == 'posts') }} href="{{ url('/backend/posts') }}">Новости</a></li>
            <li><a {{ is_active($active == 'slides') }} href="{{ url('/backend/slides') }}">Слайды</a></li>
            <li><a {{ is_active($active == 'settings') }} href="{{ url('/backend/settings') }}">Настройки</a></li>
        </ul>

        <p class="menu-label">
            Участники
        </p>
        <ul class="menu-list">
            <li><a {{ is_active($active == 'groups') }} href="{{ url('/backend/groups') }}">Группы</a></li>
            <li><a {{ is_active($active == 'users') }} href="{{ url('/backend/users') }}">Пользователи</a></li>
        </ul>

        <p class="menu-label">
            Система
        </p>
        <ul class="menu-list">
            <li><a href="{{ url('/backend/sync?cmd=wsdl-sync:data') }}">Обновить данные</a></li>
            <li><a href="{{ url('/backend/sync?cmd=wsdl-sync:discount') }}">Обновить скидки</a></li>
            <li><a href="{{ url('/backend/sync?cmd=wsdl-sync:products') }}">Обновить товары</a></li>
        </ul>
    </aside>
</div>
