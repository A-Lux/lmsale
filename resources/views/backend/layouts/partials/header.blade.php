<section id="nav" class="hero is-primary is-medium is-bold">
    <div class="hero-head">
        <header class="nav">
            <div class="container">
                <div class="nav-left">
                    <a class="nav-item" href="{{ url('/') }}">
                        <b>LiquiMoly</b>
                    </a>
                </div>

                <span class="nav-toggle">
                  <span></span>
                  <span></span>
                  <span></span>
                </span>

                <div class="nav-right">
                    <a href="{{ url('/account/signout') }}" class="nav-item is-tab">Выйти</a>
                </div>
            </div>
        </header>
    </div>
</section>