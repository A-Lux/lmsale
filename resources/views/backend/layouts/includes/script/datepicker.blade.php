@push('styles')
<link rel="stylesheet" href="/css/backend/libs/datepicker.css">
@endpush

@push('scripts')
<script src="/js/backend/libs/datepicker.js"></script>
<script>
    $(function () {
        $('input[data-toggle="datepicker"]').datepicker({
            format: 'dd.mm.yyyy'
        });
    });
</script>
@endpush
