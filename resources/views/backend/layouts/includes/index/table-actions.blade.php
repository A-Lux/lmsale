@if ( ! isset($update) || $update === true)
    <a href="{{ route("{$entity}.edit", ['id' => $id]) }}">
        <i class="fa fa-pencil-square-o"></i>
    </a>
@endif

@if ( ! isset($delete) || $delete === true)
    <form method="post" style="display: inline-block" action="{{ route("{$entity}.destroy", $id) }}">
        {{ method_field('DELETE') }}
        {{ csrf_field() }}

        <a href="javascript:void(0)"
           onclick="if(confirm('Вы уверены что хотите удалить данную запись?')) { $(this).parent().submit(); } else { return; } ">
            <i class="fa fa-trash"></i>
        </a>
    </form>
@endif
