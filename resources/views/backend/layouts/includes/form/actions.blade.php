<div class="control is-horizontal">
    <div class="control-label">
        <label class="label"></label>
    </div>
    <p class="control">
        <button type="submit" class="button is-info">{{ $action }}</button>
        <a href="{{ \URL::previous() }}" class="button is-link">Отменить</a>
    </p>
</div>
