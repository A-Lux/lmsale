<span class="select">
    <select name="{{ $attribute }}" class="input">
        <option></option>
        @foreach($options as $key => $option)
            @include('backend.layouts.includes.form.filter.dropdown-' . ((is_string($option)) ? 'array' : 'object'))
        @endforeach
    </select>
</span>
