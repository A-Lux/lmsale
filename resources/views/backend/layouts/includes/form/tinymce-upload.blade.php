<form method="post" action="{{ url('/backend/upload/tinymce') }}" enctype="multipart/form-data"
      class="is-hidden __js-tinymce__form">
    <input type="hidden" name="tinymce" value="1"/>
    <input type="file" name="image" accept="image/*" class="__js-tinymce__file"/>
</form>
