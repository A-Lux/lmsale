<input
        class="input @if ($errors->has($attribute)) is-danger @endif "
        type="text" placeholder="mm/dd/yyyy" data-toggle="datepicker"
        value="{{ old($attribute, (isset($entity) && $entity->{$attribute}) ? $entity->{$attribute}->format('m/d/Y') : null) }}"
        name="{{ $attribute }}"
>

@include('backend.layouts.includes.form.error')
