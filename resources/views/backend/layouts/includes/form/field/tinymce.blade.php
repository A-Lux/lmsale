<textarea name="{{ $attribute }}" rows="3"
          class="input __js-tinymce @if ($errors->has($attribute)) is-danger @endif">{{ old($attribute, @$entity->{$attribute}) }}</textarea>

@include('backend.layouts.includes.form.error')
