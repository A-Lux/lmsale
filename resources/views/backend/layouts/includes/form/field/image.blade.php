<span class="__is-image" data-url="{{ url("/backend/upload/picture/{$path}") }}">
    <input type="hidden" class="__js-upload-hide" name="{{ $attribute }}"
           value="{{ old($attribute, @$entity->{$attribute}) }}">

    <input class="is-hidden __js-upload-file" type="file" accept="{{ (isset($accept)) ? $accept : 'image/*' }}"/>

    <a href="#upload" class="button __js-upload">
        <span class="icon">
            <i class="fa fa-upload"></i>
        </span>
        <span>Выбрать изображение</span>
    </a>

    <a
            href="{{ old($attribute, @$entity->{$attribute}) }}" target="_blank"
            class="button is-primary __js-upload-show @if ( ! old($attribute, @$entity->{$attribute})) is-hidden @endif"
    >
        <span class="icon">
            <i class="fa fa-eye"></i>
        </span>
        <span>Открыть изображение</span>
    </a>
</span>

@include('backend.layouts.includes.form.error')
