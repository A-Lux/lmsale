<label class="checkbox">
    <input type="hidden" name="{{ $attribute }}" value="0">
    <input
            type="checkbox" name="{{ $attribute }}" value="1"
            @if(old($attribute, @$entity->{$attribute}) == 1) checked @endif
    >
    {{ $label }}
</label>

@include('backend.layouts.includes.form.error')
