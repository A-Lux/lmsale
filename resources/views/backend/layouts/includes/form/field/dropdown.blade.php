<span class="select">
    <select name="{{ $attribute }}" class="input @if ($errors->has($attribute)) is-danger @endif ">
        <option></option>
        @foreach($options as $key => $option)
            @include('backend.layouts.includes.form.field.dropdown-' . ((is_string($option)) ? 'array' : 'object'))
        @endforeach
    </select>
</span>

@include('backend.layouts.includes.form.error')
