<input
    class="input @if ($errors->has($attribute)) is-danger @endif " type="{{ isset($type) ? $type : 'text' }}"
    name="{{ $attribute }}" value="{{ old($attribute, @$entity->{$attribute}) }}"
/>

@include('backend.layouts.includes.form.error')
