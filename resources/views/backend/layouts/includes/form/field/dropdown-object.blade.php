<option value="{{ $option->id }}" @if(old($attribute, @$entity->{$attribute}) == $option->id) selected @endif>
    {{ (isset($display)) ? $option->{$display} : $option->name }}
</option>
