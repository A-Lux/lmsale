@if ( ! isset($entity))
    <input
            class="input @if ($errors->has($attribute)) is-danger @endif "
            type="password" value="{{ old($attribute) }}" name="{{ $attribute }}"
    />
@else
    <input
            class="input @if ($errors->has($attribute)) is-danger @endif "
            type="password" value="{{ old($attribute) }}" name="{{ $attribute }}"
    />

    <input type="hidden" name="{{ "old_{$attribute}" }}" value="{{ $entity->$attribute }}"/>
@endif

@include('backend.layouts.includes.form.error')
