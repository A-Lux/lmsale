<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Icon -->
    <link rel="icon" type="image/png" href="/favicon.ico"/>

    {{-- CSRF token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('section') — @yield('action') — {{ config('app.name') }}</title>

    {{-- Styles --}}
    <link href="{{ mix('/css/backend/app.css') }}" rel="stylesheet">
    @stack('styles')
</head>
<body>

<div id="app">
    @include('backend.layouts.partials.header')

    <div class="container body">
        <div class="columns">
            <div class="column">
                @if(session()->has('success'))
                    <div class="message is-success">
                        <div class="message-body">
                            {{ session()->get('success') }}
                        </div>
                    </div>
                @endif

                <div class="box content">
                    <h1 class="title is-3">
                        <b>@yield('section')</b> @yield('action')

                        <div class="pull-right">
                            @yield('actions')
                        </div>
                    </h1>

                    @yield('content')
                </div>
            </div>

            @include('backend.layouts.partials.aside')
        </div>
    </div>
</div>

@if (auth()->check() && auth()->user()->isAdmin())
    <script src="{{ mix('/js/backend/app.js') }}"></script>
    @stack('scripts')
@endif

</body>
</html>
