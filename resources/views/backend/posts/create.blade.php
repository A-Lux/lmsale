@extends('backend.layouts.app', ['active' => 'posts'])

@section('section', 'Новости')
@section('action', 'Создание')

@section('content')
    @include('backend.layouts.includes.form.tinymce-upload')

    <form method="post" action="{{ route('posts.store') }}">
        @include('backend.posts.partials.form')

        @include('backend.layouts.includes.form.actions', ['action' => 'Добавить запись'])
    </form>
@endsection
