@extends('backend.layouts.app', ['active' => 'posts'])

@section('section', 'Новости')
@section('action', 'Редактирование')

@section('content')
    @include('backend.layouts.includes.form.tinymce-upload')

    <form method="post" action="{{ route('posts.update', ['id' => $entity->id]) }}">
        {{ method_field('PATCH') }}

        @include('backend.posts.partials.form')

        @include('backend.layouts.includes.form.actions', ['action' => 'Сохранить изменения'])
    </form>
@endsection
