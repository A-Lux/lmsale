@extends('backend.layouts.app', ['active' => 'posts'])

@section('section', 'Новости')
@section('action', 'Список')

@section('actions')
    <a href="{{ route('posts.create') }}" class="button is-dark is-outlined">Добавить запись</a>
@endsection

@section('content')
    @include('backend.posts.partials.filter')

    @if(count($entities))
        <table class="table is-striped is-narrow">
            <thead>
            <tr>
                <th width="50">ID</th>
                <th width="340">GUID</th>
                <th>Название</th>
                <th>Активация</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($entities as $entity)
                <tr>
                    <td>{{ $entity->id }}</td>
                    <td>{{ $entity->guid }}</td>
                    <td>{{ $entity->name }}</td>
                    <td>{{ $entity->is_active ? 'Да' : 'Нет' }}</td>
                    <td class="is-icon">
                        @include('backend.layouts.includes.index.table-actions', [
                            'entity' => 'posts',
                            'id' => $entity->id,
                        ])
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $entities->appends(paginateAppends())->links() }}
    @else
        @include('backend.layouts.includes.index.no-results')
    @endif
@endsection