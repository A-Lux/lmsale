@component('mail::message')
# Регистрация на портале

## Данные
* Ф.И.О.: **{{ $user->name }}**
* E-mail: **{{ $user->email }}**
* Пароль: **{{ $password }}**
* ИИН: **{{ $user->iin }}**

С уважением,<br/>
{{ config('app.name') }}
@endcomponent
