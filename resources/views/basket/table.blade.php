@extends('layouts.app', [
    'breadcrumbs' => [
        '/' => 'Главная',
        '/basket' => 'Корзина',
    ],
])

@section('title', 'Корзина')

@section('content')
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" integrity="sha256-3blsJd4Hli/7wCQ+bmgXfOdK7p/ZUMtPXY08jmxSSgk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" integrity="sha256-ENFZrbVzylNbgnXx0n3I1g//2WeO47XxoPe0vkp3NC8=" crossorigin="anonymous" />
<script>
	if(window.location.hash == "#shortage"){
		toastr.error('Товара нет на складе', 'Ошибка')
	}
</script>
    <section class="section-main">
        <div class="container">
            @include('layouts.partials.aside')

            <section class="content">
                <div class="cart-page">
                    <div class="cart-steps">
                        <div class="cart-steps-item is-active">1. Корзина</div>
                        <div class="cart-steps-item">2. Доставка и оплата</div>
                    </div>

                    <div class="cart-info">
                        <div class="cart-page-title">
                            <span></span>Корзина
                        </div>

                        <div class="cart-info-details">
                            <div class="cart-info-quantity cart-info-item">
                                Товаров в корзине: <span>{{ number_format(count($baskets), 0, '.', ' ') }} </span>
                            </div>

                            <div class="cart-info-summ cart-info-item">
                                На общую сумму: <span>{{ $amount }} ₸</span>
                            </div>
                        </div>
                    </div>

                    @if (count($baskets))
                        <form action="{{ url('/preorder') }}" method="post">
                            {{ csrf_field() }}

                            <div class="cart-table">
                                <div class="cart-table-header cart-table-row">
                                    <div class="cart-table-cell cart-table-cell-name">Наименование</div>
                                    <div class="cart-table-cell cart-table-cell-quantity">Кол-во</div>
                                    <div class="cart-table-cell cart-table-cell-price">Цена</div>
                                    <div class="cart-table-cell cart-table-cell-Шsumm">Сумма</div>
                                    <div class="cart-table-cell cart-table-cell-ctrl"></div>
                                </div>

                                @each('basket.table-item', $baskets, 'basket')

                                <div class="cart-table-row cart-table-footer">
                                    <div class="cart-table-cell cart-table-cell-name"></div>
                                    <div class="cart-table-cell cart-table-cell-quantity"></div>
                                    <div class="cart-table-cell cart-table-cell-price"></div>
                                    <div class="cart-table-cell cart-table-cell-summ">
                                        Итог: <br/> <span>{{ $amount }}</span> ₸
                                    </div>
                                    <div class="cart-table-cell cart-table-cell-ctrl"></div>
                                </div>
                            </div>

                            <div style="background: #cdcdcd; padding: 16px; margin-top: 16px">
                                @php $order_sum = (int)str_replace(' ', '', auth()->user()->basket()['amount']) @endphp
                                @php $total_amount =(int) str_replace(' ', '', $amount) @endphp

                                @foreach($baskets as $basket)
                                    @if ( ! $basket->product->has_no_sale)
                                        @if ($discount = app('product-storage')->getDiscountOrder($basket->product, $order_sum))
                                            <li><strong>Скидка -{{ $discount }}%</strong> на товар {{ $basket->product->name }}</li>

                                            @php $total_amount = $total_amount - (($discount / 100) * ($basket->product->price_discount * $basket->count)) @endphp
                                        @endif
                                    @endif
                                @endforeach

                                <h1 style="margin-bottom: 0; margin-top: 12px">Общая сумма: {{ number_format($total_amount, 0, '.', ' ') }} ₸</h1>
                            </div>

                            <div class="cart-confirm">
                                <button class="btn-confirm btn-cart btn-cart-arrow" type="submit">
                                    Оформить заказ
                                </button>
                            </div>
                        </form>
                    @else
                        <br><br>

                        <div class="empty">
                            <h3>
                                {{
                                    auth()->user()->is_active
                                        ? 'Ваша корзина пуста'
                                        : 'Ваша компания еще не прошла модерацию, для соверщения покупок'
                                }}
                            </h3>
                        </div>
                    @endif
                </div>
            </section>
        </div>
    </section>
@endsection