<div class="cart-table-row cart-table-row-js">
    <div class="cart-table-cell cart-table-cell-name cart-table-cont">
        <div class="cart-table-img">
            <a href="{{ $basket->product->link() }}"><img src="{{ $basket->product->image }}"></a>
        </div>

        <div class="cart-table-desc">
            <div class="cart-table-title">
                <a href="{{ $basket->product->link() }}">{{ $basket->product->name }}</a>
            </div>

            <div class="cart-table-article">
                Артикул: {{ $basket->product->code }}
            </div>
        </div>
    </div>

    <div class="cart-table-cell cart-table-cell-quantity">
        <div class="quantity-ctrl">
            <div class="quantity-incr"></div>
            <input class="quantity-digit" type="text" max="{{ $basket->product->count_order }}" name="basket[{{ $basket->id }}]" value="{{ $basket->count }}"/>
            <div class="quantity-dicr"></div>
        </div>
    </div>

    <div class="cart-table-cell cart-table-cell-price">
        <span>{{ number_format($basket->product->price_discount, 0, '.', ' ') }}</span> ₸
    </div>
    <div class="cart-table-cell cart-table-cell-summ">
        <span>{{ $basket->amount() }}</span> ₸
    </div>

    <div class="cart-table-cell cart-table-cell-ctrl">
        <a class="cart-row-destroy" href="{{ $basket->linkDelete() }}"></a>
    </div>
</div>
