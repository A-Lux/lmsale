<div class="post">
    <div class="post-img">
        <a href="{{ $post->link() }}"><img src="{{ $post->image }}"/></a>
    </div>

    <div class="post-body">
        <h2>
            <a href="{{ $post->link() }}">{{ $post->name }}</a>
        </h2>

        {!! $post->description !!}
    </div>

    <div class="clearfix"></div>
</div>
