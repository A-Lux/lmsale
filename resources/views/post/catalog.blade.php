@extends('layouts.app', [
    'active' => 'posts',
    'breadcrumbs' => [
        '/' => 'Главная',
        '/posts' => 'Новости компании',
    ]
])

@section('title', 'Новости компании')

@section('content')
    <section class="section-main">
        <div class="container">
            @include('layouts.partials.aside')

            <section class="content">
                <h1>Новости компании</h1>

                <div class="posts">
                    @each('post.catalog-item', $posts, 'post')
                </div>

                <div class="catalog-pagination">
                    {{ $posts->appends(paginateAppends())->links() }}
                </div>
            </section>
        </div>
    </section>
@endsection