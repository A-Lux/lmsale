@extends('layouts.app', [
    'active' => 'posts',
    'breadcrumbs' => [
        '/' => 'Главная',
        '/posts' => 'Новости компании',
        $post->link() => $post->name,
    ]
])

@section('title', $post->name)

@section('content')
    <section class="section-main">
        <div class="container">
            @include('layouts.partials.aside')

            <section class="content">
                <h1>{{ $post->name }}</h1>

                    <img src="{{ $post->image }}" class="post-image">

                {!! $post->content !!}
            </section>
        </div>
    </section>
@endsection