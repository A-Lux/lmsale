<div class="hidden">
    <div id="registration">
        <div class="login-main-block">
            <form class="login-main">
                <div class="login-main-title">Регистрация</div>
                <div class="login-main-row">
                    <label class="login-main-label" for="name">Имя: </label>
                    <input class="login-main-input" type="text" id="name" required="true"/>
                </div>
                <div class="login-main-row">
                    <label class="login-main-label" for="surname">Фамилия: </label>
                    <input class="login-main-input" type="text" id="surname" required="true"/>
                </div>
                <div class="login-main-row">
                    <label class="login-main-label" for="city">Город: </label>
                    <input class="login-main-input" type="text" id="city" required="true"/>
                </div>
                <div class="login-main-row">
                    <label class="login-main-label" for="address">Адрес: </label>
                    <input class="login-main-input" type="text" id="address" required="true"/>
                </div>
                <div class="login-main-row">
                    <label class="login-main-label" for="tel">Телефон: </label>
                    <input class="login-main-input" type="text" id="tel" required="true"/>
                </div>
                <div class="login-main-row">
                    <label class="login-main-label" for="email">E-mail: </label>
                    <input class="login-main-input" type="text" id="email" required="true"/>
                </div>
                <div class="login-main-btns"><a class="login-btn login-btn-signup fancybox" href="#registration">Зарегистрироваться</a><a class="login-btn login-btn-esc" href="#">Отмена</a></div>
            </form>
        </div>
    </div>
</div>
