<header class="top @if(auth()->guest()) top-main @endif ">
    <div class="top-header">
        <div class="container">
            <div class="top-logo collect-in">
                <a href="{{ url('/') }}"><img src="/img/logo.png"/></a>
            </div>

            @if (auth()->guest())
                <div class="col-md-8">
                    <div class="top-slogan">“The best brand of oil in Germany is LIQUI MOLY ”</div>

                    <div class="top-menu-wrap">
                        <nav class="top-menu hidden-xs collect-in">
                            @include('layouts.partials.menu')
                        </nav>
                    </div>
                </div>
            @else
                <div class="top-cart-wrapper">
                    <div class="top-cart-user">
                        <a href="{{ url('/account') }}">
                            {{ auth()->user()->name }}
                        </a>
                        @if (auth()->user()->group_id == \App\Member\Group::ADMINISTRATOR)
                            /
                            <a href="{{ url('/backend') }}">
                                Админ-панель
                            </a>
                        @endif
                    </div>

                    <div class="top-cart">
                        <div class="top-cart-desc">
                            <div class="top-cart-quant">
                                Товаров в корзине:
                                <span>{{ auth()->user()->basket()['total'] }}</span>
                            </div>

                            <div class="top-cart-summ">
                                На общую сумму:
                                <span>{{ auth()->user()->basket()['amount'] }}</span> ₸
                            </div>
                        </div>

                        <div class="top-cart-btn">
                            <a class="btn-cart btn-cart-arrow" href="{{ url('/basket') }}">Перейти к заказу</a>
                        </div>
                    </div>
                </div>

                <div class="top-menu-wrap">
                    <nav class="top-menu hidden-xs collect-in">
                        @include('layouts.partials.menu')
                    </nav>
                </div>
            @endif
        </div>
    </div>

    @if (isset($breadcrumbs) && count($breadcrumbs))
        <div class="top-crumbs-block">
            <div class="container">
                <nav class="top-crumbs">
                    @foreach($breadcrumbs as $href => $name)
                        <a href="{{ $href }}">{{ $name }}</a>
                    @endforeach
                </nav>
            </div>
        </div>
    @endif
</header>
