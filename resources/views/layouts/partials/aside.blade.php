<aside class="sidebar">
    <div class="search-block">
        <form class="search-article search-row" method="get" action="{{ url('/products/search') }}">
            <input type="text" name="code" value="{{ @$_GET['code'] }}" placeholder="Поиск по артикулу"/>
            <button class="search-btn"></button>
        </form>
    </div>

    <div class="catalog-menu-block">
        <div class="catalog-menu-title">Каталог товаров</div>

        <ul class="catalog-menu">
            @foreach(\App\Catalog\Category::whereNull('parent_guid')->sorted()->activated()->get() as $parent)
                <li class="catalog-menu-item @if(count($parent->childs())) has-child @endif {{ is_active(@$active_parent_id == $parent->guid, false) }}">
                    <a href="{{ $parent->link() }}">{{ $parent->name }}</a>
                    <ul>
                        @foreach($parent->childs() as $child)
                            <li {{ is_active(@$active_child_id == $child->guid) }}>
                                <a href="{{ $child->link() }}">{{ $child->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                </li>
            @endforeach
        </ul>
    </div>
</aside>
