<ul>
    <li {{ is_active(@$active == 'home') }}><a href="{{ url('/') }}">Главная</a></li>
    @if (auth()->check())
        <li {{ is_active(@$active == 'products') }}><a href="{{ url('/products') }}">Товары</a></li>
    @endif
    <li {{ is_active(@$active == 'posts') }}><a href="{{ url('/posts') }}">Новости</a></li>
    <li {{ is_active(@$active == 'contacts') }}><a href="{{ url('/page/contacts') }}">Контакты</a>
    </li>
</ul>
