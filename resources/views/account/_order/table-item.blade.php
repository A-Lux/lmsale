<div class="cabinet-table-row">
    <div class="cabinet-table-cell cabinet-table-article">{{ $order->id }}</div>
    <div class="cabinet-table-cell">{{ $order->created_at->format('d.m.Y') }}</div>
    <div class="cabinet-table-cell">{{ $order->amount() }} ₸</div>
    <div class="cabinet-table-cell cabinet-table-link">
        <a href="{{ $order->link() }}">Подробнее</a><br />
        <a href="{{ $order->link() }}/document?created=0">Загрузить документ без счета</a><br />
        <a href="{{ $order->link() }}/document?created=1">Загрузить документ со счетом</a>
    </div>
</div>
