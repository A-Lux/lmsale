<h2>Мои заказы</h2>

@if (count($orders))
    <div class="cabinet-table">
        <div class="cabinet-table-row cabinet-table-header">
            <div class="cabinet-table-cell cabinet-table-article">Заказ, №</div>
            <div class="cabinet-table-cell">Дата</div>
            <div class="cabinet-table-cell">Сумма</div>
            <div class="cabinet-table-cell cabinet-table-empty"></div>
        </div>

        @each('account._order.table-item', $orders, 'order')
    </div>
@else
    <div class="empty">
        <h3>Ваша история заказов пуста</h3>
    </div>
@endif

