@extends('layouts.app', [
    'breadcrumbs' => [
        '/' => 'Главная',
        '/account' => 'Личный кабинет',
    ]
])

@section('title', 'Личный кабинет')

@section('content')
    <section class="section-main">
        <div class="container">
            @include('layouts.partials.aside')

            <section class="content">
                <section class="cabinet-page">
                    <div class="cabinet-header">
                        <h1>Личный кабинет</h1>

                        <div class="cabinet-info">
                            <div class="cabinet-info-actions">
                                <div class="cabinet-info-item">
                                    <a href="{{ url('/settings/password') }}">Сменить пароль</a>
                                </div>

                                <div class="cabinet-info-item">
                                    <a href="{{ url('/account/signout') }}">Выход</a>
                                </div>
                            </div>

                            <div class="cabinet-name">
                                {{ $user->name }}
                            </div>

                            <div class="cabinet-info-item">
                                ИИН/БИН {{ $user->iin }}
                            </div>

                            <div class="cabinet-info-item">
                                {{ $user->email }}
                            </div>
                        </div>
                    </div>

                    @include('account._order.table')
                </section>
            </section>
        </div>
    </section>
@endsection