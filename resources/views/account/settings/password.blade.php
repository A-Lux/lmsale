@extends('layouts.app', [
    'breadcrumbs' => [
        '/' => 'Главная',
        '/account' => 'Личный кабинет',
        '/settings/password' => 'Смена пароля',
    ],
])

@section('title', 'Смена пароля')

@section('content')
    <section class="section-main">
        <div class="container">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="auth">
                    <form action="{{ url('/settings/password') }}" method="post">
                        <h1>Смена пароля</h1>

                        <div class="field">
                            <input type="password" name="current_password" placeholder="Текущий пароль" required autofocus>

                            @if($errors->has('current_password'))
                                <span class="error">
                                    {{ $errors->first('current_password') }}
                                </span>
                            @endif
                        </div>

                        <hr>

                        <div class="field">
                            <input type="password" name="new_password" placeholder="Новый пароль" required>

                            @if($errors->has('new_password'))
                                <span class="error">
                                    {{ $errors->first('new_password') }}
                                </span>
                            @endif
                        </div>

                        <div class="field">
                            <input type="password" name="new_password_confirmation" placeholder="Повторите новый пароль" required>

                            @if($errors->has('new_password_confirmation'))
                                <span class="error">
                                    {{ $errors->first('new_password_confirmation') }}
                                </span>
                            @endif
                        </div>

                        {{ csrf_field() }}

                        <button type="submit">Сменить пароль</button>
                    </form>
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>
    </section>
@endsection
