@extends('layouts.app', [
    'breadcrumbs' => [
        '/' => 'Главная',
        '/account' => 'Личный кабинет',
        '/settings/email' => 'Смена email',
    ],
])

@section('title', 'Смена email')

@section('content')
    <section class="section-main">
        <div class="container">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="auth">
                    <form action="{{ url('/settings/email') }}" method="post">
                        <h1>Смена email</h1>

                        <div class="field">
                            <input type="password" name="current_password" placeholder="Текущий пароль" required autofocus>

                            @if($errors->has('current_password'))
                                <span class="error">
                                    {{ $errors->first('current_password') }}
                                </span>
                            @endif
                        </div>

                        <hr>

                        <div class="field">
                            <input type="email" name="email" placeholder="Новый email" value="{{ old('email') }}" required>

                            @if($errors->has('email'))
                                <span class="error">
                                    {{ $errors->first('email') }}
                                </span>
                            @endif
                        </div>

                        {{ csrf_field() }}

                        <button type="submit">Сменить email</button>
                    </form>
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>
    </section>
@endsection
