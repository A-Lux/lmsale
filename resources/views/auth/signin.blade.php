@extends('layouts.app', [
    'breadcrumbs' => [
        '/' => 'Главная',
        '/auth/signun' => 'Вход',
    ],
])

@section('title', 'Вход')

@section('content')
    <section class="section-main">
        <div class="container">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="auth">
                    <form action="{{ url('/auth/signin') }}" method="post">
                        <h1>Вход</h1>

                        <div class="field">
                            <input type="text" name="iin" placeholder="ИИН/БИН" value="{{ old('iin') }}" required autofocus>

                            @if ($errors->has('iin'))
                                <span class="error">{{ $errors->first('iin') }}</span>
                            @endif
                        </div>

                        <div class="field">
                            <input type="password" name="password" placeholder="Пароль" value="{{ old('password') }}" required>

                            @if ($errors->has('password'))
                                <span class="error">{{ $errors->first('password') }}</span>
                            @endif
                        </div>

                        <div class="field pull-right">
                            <br><a href="{{ url('/auth/password/restore') }}">Забыл пароль?</a>
                        </div>

                        {{ csrf_field() }}

                        <button type="submit">Войти</button>
                    </form>
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>
    </section>
@endsection
