@extends('layouts.app', [
    'breadcrumbs' => [
        '/' => 'Главная',
        '/auth/signin' => 'Вход',
        '/auth/password/recovery' => 'Смена пароля',
    ],
])

@section('title', 'Смена пароля')

@section('content')
    {{ print_r($errors) }}
    <section class="section-main">
        <div class="container">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="auth">
                    <form action="{{ url('/auth/password/recovery') }}" method="post">
                        <h1>Смена пароля</h1>

                        <div class="field">
                            <input type="email" name="email" placeholder="Email" required autofocus>

                            @if ($errors->has('email'))
                                <span class="error">{{ $errors->first('email') }}</span>
                            @endif
                        </div>

                        <div class="field">
                            <input type="password" name="password" placeholder="Новый пароль" required>

                            @if ($errors->has('password'))
                                <span class="error">{{ $errors->first('password') }}</span>
                            @endif
                        </div>

                        <div class="field">
                            <input type="password" name="password_confirmation" placeholder="Повторите пароль" required>

                            @if ($errors->has('password_confirmation'))
                                <span class="error">{{ $errors->first('password_confirmation') }}</span>
                            @endif
                        </div>

                        <input type="hidden" name="token" value="{{ $token }}">

                        {{ csrf_field() }}

                        <button type="submit">Сменить пароль</button>
                    </form>
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>
    </section>
@endsection