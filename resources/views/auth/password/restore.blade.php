@extends('layouts.app', [
    'breadcrumbs' => [
        '/' => 'Главная',
        '/auth/signin' => 'Вход',
        '/auth/password/restore' => 'Восстановление пароля',
    ],
])

@section('title', 'Восстановление пароля')

@section('content')
    <section class="section-main">
        <div class="container">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="auth">
                    <form action="{{ url('/auth/password/restore') }}" method="post">
                        <h1>Восстановление пароля</h1>

                        @if(session('status'))
                            <h3 style="color: green">{{ session('status') }}</h3>
                        @endif

                        <div class="field">
                            <input type="email" name="email" placeholder="Email" value="{{ old('email') }}" required
                                   autofocus>

                            @if ($errors->has('email'))
                                <span class="error">{{ $errors->first('email') }}</span>
                            @endif
                        </div>

                        {{ csrf_field() }}

                        <button type="submit">Восстановить пароль</button>
                    </form>
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>
    </section>
@endsection