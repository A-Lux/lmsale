@extends('layouts.app', [
    'active' => 'home',
])

@section('title', 'Главная')

@section('content')
    <section class="carousel-main">
        <div class="container">
            <div class="row">
                @if (auth()->check())
                    <div class="col-sm-3"></div>
                @endif

                <div class="col-sm-6 col-xs-12">
                    <div class="carousel" data-s2show="1" data-autoplay="true">
                        <div class="carousel-inner">
                            @foreach($slides as $slide)
                                <div class="carousel-item"><img src="{{ $slide->image }}"/></div>
                            @endforeach
                        </div>

                        <div class="carousel-nav">
                            @foreach($slides as $slide)
                                <div class="carousel-nav-item"></div>
                            @endforeach
                        </div>
                    </div>
                </div>

                @if (auth()->guest())
                    <div class="col-sm-6 col-xs-12">
                        <div class="login-main-block">
                            <form class="login-main" action="{{ url('/auth/signin') }}" method="post">
                                <div class="login-main-title">Вход в личный кабинет</div>

                                <input class="login-main-input" type="text" value="{{ old('iin') }}" name="iin"
                                       placeholder="ИИН/БИН" required autofocus/>

                                @if($errors->has('iin'))
                                    <span class="error">{{ $errors->first('iin') }}</span>
                                @endif

                                <input class="login-main-input" type="password" placeholder="Пароль" name="password"
                                       required/>

                                @if($errors->has('password'))
                                    <span class="error">{{ $errors->first('password') }}</span>
                                @endif

                                {{ csrf_field() }}

                                <div class="login-main-btns">
                                    <button type="submit" class="login-btn login-btn-signup">Войти</button>
                                </div>

                                <div class="login-forget">
                                    <a href="{{ url('/auth/password/restore') }}">Забыли пароль или логин?</a>
                                </div>
                            </form>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>

    <section class="news-main">
        <div class="container">
            <h2 class="news-main-title">Новости компании</h2>
            <div class="carousel news-main-carousel" data-s2show="3">
                <div class="carousel-inner">
                    @foreach($posts as $post)
                        <div class="carousel-item">
                            <div class="news-main-item">
                                <div class="news-main-thumb">
                                    <a href="{{ $post->link() }}"><img src="{{ $post->image }}"></a>
                                </div>

                                <div class="news-main-cont">
                                    <div class="news-main-excerpt">
                                        {{ $post->name }}
                                    </div>
                                    <div class="news-main-text">
                                        {!! $post->description !!}
                                    </div>
                                </div>

                                <div class="news-main-link">
                                    <a href="{{ $post->link() }}">Подробнее</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="carousel-arrows side">
                    <div class="carousel-arrow left"></div>
                    <div class="carousel-arrow right"></div>
                </div>
            </div>

            <div class="news-main-all">
                <a href="{{ url('/posts') }}">Все новости</a>
            </div>
        </div>
    </section>
@endsection