@extends('layouts.app', [
    'active' => 'contacts',
    'breadcrumbs' => [
        '/' => 'Главная',
        '/page/contacts' => $page->name,
    ],
])

@section('title', $page->name)

@section('content')
    <section class="section-main">
        <div class="container">
            @include('layouts.partials.aside')

            <section class="content">
                <div class="contacts-page">
                    <h1>Контакты</h1>

                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="ft-cont-div">
                                <div class="ft-cont-icon"></div>
                                <div class="ft-cont-desc">
                                    @foreach(explode(',', app('Settings')->get('contacts.phones')) as $phone)
                                        <a href="tel:{{ $phone }}">{{ $phone }}</a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="ft-cont-div">
                                <div class="ft-cont-icon ft-cont-icon-addr"></div>
                                <div class="ft-cont-desc">
                                    <div class="ft-addr">
                                        <div class="ft-addr-title">Адрес:</div>
                                        <div class="ft-addr-text">{{ app('Settings')->get('contacts.address_regular') }}</div>
                                    </div>
                                    <div class="ft-addr">
                                        <div class="ft-addr-title">Фактический адрес:</div>
                                        <div class="ft-addr-text">{{ app('Settings')->get('contacts.address_actual') }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="ft-socs">
                                <a class="ft-socs-item socs-fb"
                                   href="{{ app('Settings')->get('social.facebook') }}"></a>
                                <a class="ft-socs-item socs-twit"
                                   href="{{ app('Settings')->get('social.twitter') }}"></a>
                                <a class="ft-socs-item socs-qu"
                                   href="{{ app('Settings')->get('social.quora') }}"></a>
                                <a class="ft-socs-item-link"
                                   href="{{ app('Settings')->get('contacts.email') }}">{{ app('Settings')->get('contacts.email') }}</a>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="ft-map-title ft-cont-title">Наше местоположение:</div>
                            <div class="ft-map-block"><script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=DPBmE1HoeWk6K1jhdDWFs_i4pLEdP62P&amp;width=100%&amp;height=100%&amp;lang=ru_RU&amp;sourceType=constructor&amp;scroll=true"></script></div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection
