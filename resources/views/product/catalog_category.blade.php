@extends('layouts.app', [
    'active' => 'products',
    'breadcrumbs' => [
        '/' => 'Главная',
        '/products' => 'Каталог',
        $category->link() => $category->name,
    ],
])

@section('title', 'Каталог товаров')

@section('content')
    <section class="section-main">
        <div class="container">
            @include('layouts.partials.aside', [
                'active_parent_id' => ($category->parent()) ? $category->parent_guid : $category->guid,
                'active_child_id' => ($category->parent()) ? $category->guid : null,
            ])

            <section class="content">
                <h1>Каталог товаров</h1>

                <div class="catalog-list">
                    @each('product.catalog-item', $products, 'product', 'product.catalog_empty')
                </div>

                <div class="catalog-pagination">
                    {{ $products->appends(paginateAppends())->links() }}
                </div>
            </section>
        </div>
    </section>
@endsection