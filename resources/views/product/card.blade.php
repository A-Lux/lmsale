@extends('layouts.app', [
    'active' => 'products',
    'breadcrumbs' => [
        '/' => 'Главная',
        '/products' => 'Товары',
        $category->link() => $category->name,
        $product->link() => $product->name,
    ],
])

@section('title', $product->name)

@section('content')
    <section class="section-main">
        <div class="container">
            @include('layouts.partials.aside')

            <section class="content">
                <div class="product-page-wrapper">
                    <article class="product-page">
                        <div class="product-img">
                            <div class="carousel" data-s2show="1" data-autoplay="true">
                                <div class="carousel-inner">
                                    <div class="carousel-item">
                                        <img src="{{ $product->image }}"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="product-cont">
                            <header class="product-header">
                                <h1 class="product-title">{{ $product->name }}</h1>
                                <div class="product-article">Артикул: {{ $product->code }}</div>
                                <div class="product-article">В наличии: {{ $product->count_total ? 'Да' : 'Нет' }}</div>
                            </header>

                            <main class="product-text">
                                {!! $product->content !!}
                            </main>
                        </div>

                        <footer class="product-ctrl">
                            <div class="product-special">
                                @if ($product->is_line_top || $product->is_line_profy)
                                    Специальная линейка:

                                    @if ($product->is_line_top)
                                        <span class="product-special-icon product-special-top"></span>
                                    @endif

                                    @if ($product->is_line_profy)
                                        <span class="product-special-icon product-special-prof"></span>
                                    @endif
                                @endif
                            </div>

                            <div class="product-price catalog-list-item-price">
                                {{ number_format($product->price_discount, 0, '.', ' ') }} ₸
                            </div>

                            <div class="product-quantity">
                                @if (auth()->check() && auth()->user()->is_active)
                                    <form class="catalog-list-item-buy" action="{{ url('/basket/add') }}" method="post">
                                        {{ csrf_field() }}

                                        <input type="hidden" name="product_id" value="{{ $product->id }}">

                                        <div class="quantity-ctrl">
                                            <div class="quantity-incr"></div>
                                            <input class="quantity-digit" type="text" max="{{ $product->count_order }}" name="count" value="1">
                                            <div class="quantity-dicr"></div>
                                        </div>

                                        <button type="submit" class="btn-cart">Добавить в корзину</button>
                                    </form>
                                @endif
                            </div>
                        </footer>
                    </article>

                    @if (
                        $product->about_properties ||
                        $product->about_application ||
                        $product->about_packing ||
                        $product->about_documentation ||
                        $product->about_analogs
                    )
                        <div class="product-params">
                            <div class="product-tabs tab-wrap-link">
                                @if ($product->about_properties)
                                    <div class="product-tabs-item tab-item is-active">Свойства</div>
                                @endif

                                @if ($product->about_application)
                                    <div class="product-tabs-item tab-item">Применение</div>
                                @endif

                                @if ($product->about_packing)
                                    <div class="product-tabs-item tab-item">Фасовка</div>
                                @endif

                                @if ($product->about_documentation)
                                    <div class="product-tabs-item tab-item">Документация</div>
                                @endif

                                @if ($product->about_analogs)
                                    <div class="product-tabs-item tab-item">Аналоги</div>
                                @endif

                            </div>

                            <div class="product-params-desc tab-wrap-cont">
                                @if ($product->about_properties)
                                    <article class="product-params-text tab-item-cont is-active">
                                        {!! $product->about_properties !!}
                                    </article>
                                @endif

                                @if ($product->about_application)
                                    <article class="product-params-text tab-item-cont">
                                        {!! $product->about_application !!}
                                    </article>
                                @endif

                                @if ($product->about_packing)
                                    <article class="product-params-text tab-item-cont">
                                        {!! $product->about_packing !!}
                                    </article>
                                @endif

                                @if ($product->about_documentation)
                                    <article class="product-params-text tab-item-cont">
                                        {!! $product->about_documentation !!}
                                    </article>
                                @endif

                                @if ($product->about_analogs)
                                    <article class="product-params-text tab-item-cont">
                                        {!! $product->about_analogs !!}
                                    </article>
                                @endif
                            </div>
                        </div>
                    @endif
                </div>
            </section>
        </div>
    </section>
@endsection