@extends('layouts.app', [
    'active' => 'products',
    'breadcrumbs' => [
        '/' => 'Главная',
        '/products' => 'Каталог',
        '/products/search' => 'Поиск',
    ],
])

@section('title', 'Поиск товаров')

@section('content')

<section class="section-main">
    <div class="container">
        @include('layouts.partials.aside')

        <section class="content">
            <h1>Поиск товаров</h1>

            <div class="catalog-list">
                @each('product.catalog-item', $products, 'product', 'product.catalog_empty')
            </div>

            <div class="catalog-pagination">
                {{ $products->appends($_GET)->links() }}
            </div>
        </section>
    </div>
</section>
@endsection