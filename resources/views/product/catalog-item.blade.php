<div class="catalog-list-item">
    <div class="catalog-list-item-img">
        <a href="{{ $product->link() }}"><img src="{{ $product->image }}"></a>
    </div>

    <div class="catalog-list-item-content">
        <h3 class="catalog-list-item-title">
            <a href="{{ $product->link() }}">{{ $product->name }}</a>
        </h3>

        <div class="catalog-list-item-desc">
            {{ strip_tags($product->description) }}
        </div>

        <div class="catalog-list-item-meta">
            <div class="catalog-list-item-article">
                Артикул: <span>{{ $product->code }}</span>
            </div>
            <div class="catalog-list-item-article">
                Есть в наличии: <span>{{ $product->count_total ? 'Да' : 'Нет' }}</span>
            </div>
        </div>
    </div>

    <div class="catalog-list-item-ctrl">
        <div class="catalog-list-item-link">
            <a href="{{ $product->link() }}">Подробнее</a>
        </div>

        @if (auth()->check() && auth()->user()->canBuy())
            <form class="catalog-list-item-buy" action="{{ url('/basket/add') }}" method="post">
                {{ csrf_field() }}

                <input type="hidden" name="product_id" value="{{ $product->id }}">

                <div class="quantity-ctrl">
                    <div class="quantity-incr"></div>
                    <input class="quantity-digit" type="text" name="count" value="1">
                    <div class="quantity-dicr"></div>
                </div>

                <button type="submit" class="btn-cart">Добавить в корзину</button>
            </form>
        @endif

        <div class="catalog-list-item-price">{{ number_format($product->price_discount, 0, '.', ' ') }} ₸</div>
    </div>
</div>
