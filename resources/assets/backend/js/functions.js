$(function () {

    autosize($('textarea'));

    $('form').on('keyup keypress', function (e) {
        e = e || event;
        var txtArea = /textarea/i.test((e.target || e.srcElement).tagName);
        return txtArea || (e.keyCode || e.which || e.charCode || 0) !== 13;
    });

    var uploader;

    $('.__js-upload').on('click', function (e) {
        e.preventDefault();

        // find parent block
        uploader = $(this).parent();
        uploader.find('.__js-upload-file').click();
    });

    $('.__js-upload-file').on('change', function () {
        var
            fileinput = $(this),
            button = uploader.find('.__js-upload');

        button.addClass('is-loading');

        var formData = new FormData();
        formData.append('file', fileinput.get(0).files[0]);

        $.ajax({
            type: 'POST',
            url: uploader.data('url'),
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                button.removeClass('is-loading');

                fileinput.val('');
                uploader.find('.__js-upload-hide').val(response);

                var show = uploader.find('.__js-upload-show');
                show.removeClass('is-hidden');
                show.attr('href', response);
            }
        })
    });

    var modals = {
        launch: $('.__js-modal__launch'),
        close: $('.__js-modal__close'),
        class: 'is-active',
        active: null,
        hideAjax: function () {
            modals.active.find('.__main').removeClass('is-hidden');
            modals.active.find('.__ajax').addClass('is-hidden').html('');
            modals.active.find('input, select').each(function() {
                $(this).val('');
            });
        },
        showAjax: function (content) {
            modals.active.find('.__main').addClass('is-hidden');
            modals.active.find('.__ajax').removeClass('is-hidden').html(content);
        }
    };

    modals.close.on('click', function (e) {
        e.preventDefault();

        if (modals.active) {
            modals.active.removeClass(modals.class);
            modals.hideAjax();
        }
    });

    modals.launch.on('click', function (e) {
        e.preventDefault();

        modals.active = $('#modal-' + $(this).data('for'));
        modals.active.addClass(modals.class);
    });

    var imports = {
        button: $('.__js-import'),
        select: $('.__js-import__school'),
        counts: $('.__js-import__counts'),
    }

    imports.select.on('change', function() {
        if ($(this).val()) {
            imports.button.removeClass('is-disabled');
        } else {
            imports.button.addClass('is-disabled');
        }
    });

    imports.button.on('click', function () {
        imports.button.addClass('is-loading');

        var counts = {};

        $('.__js-count').each(function() {
            if ($(this).val()) {
                var group_id = $(this).attr('name');
                counts[group_id] = $(this).val();
                $(this).val('');
            }
        });

        $.ajax({
            type: 'POST',
            url: '/ru/admin/ajax/accounts',
            data: {
                school_id : imports.select.val(),
                counts : counts
            },
            success: function (response) {
                imports.button.removeClass('is-loading').addClass('is-disabled');
                imports.select.val('');
                modals.showAjax(response);
            }
        });
    });
});