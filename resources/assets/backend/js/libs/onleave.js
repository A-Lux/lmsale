window.forWasChanged = false;
window.forWasSubmitted = false;

$(document).on(
    'change',
    'form input, form textarea, form select',
    function () {
        forWasChanged = true;
    }
);

$(document).ready(function () {
    window.onbeforeunload = function (e) {
        if (window.forWasChanged && ! window.forWasSubmitted) {
            var message = 'Вы не сохранили свои изменения.', e = e || window.event;

            if (e) {
                e.returnValue = message;
            }

            return message;
        }
    }

    $('form').submit(function () {
        window.forWasSubmitted = true;

        $('form button[type=submit]').attr('disabled', true);
    });
});
