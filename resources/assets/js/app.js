jQuery(document).ready(function($) {
    if($('.delivery-type-select').length){
        let $selected = $('.delivery-type-selected'),
            $dropdown = $('.delivery-type-dropdown');

        $dropdown.each(function(index, el) {
            $(this).click(function(){
                $(this).parent().find('.delivery-type-selected').trigger('click');
            })
        });
        $selected.each(function(index, el) {
            let $selectedOne = $(this);
            $selectedOne.click(function(){
                let $options = $(this).parent().find('.delivery-type-options');
                $options.hasClass('open') ? $options.removeClass('open') : $options.addClass('open');
            });

            let $optionItems =  $selectedOne.parent().find('.delivery-type-options-item');

            $optionItems.each(function(index, el) {
                $(this).click(function(){
                    console.log()
                    if($selectedOne.hasClass('placeholder')){

                    }else{
                        $selectedOne.trigger('click');
                        $selectedOne.text($(this).text());
                        $('input[name=user_id]').val($(this).data('id'));
                    }

                })
            });
        });
    }

    $('.fancybox').fancybox();

    if ($('.carousel').length) {
        var $carousels = $('.carousel');

        $carousels.each(function(index, el) {
            //
            // set some vars
            //
            var $carBlock = $(this),
                $s2show = $carBlock.data('s2show') ? breakpoints($carBlock.data('s2show')) : breakpoints(4),
                autoplay = false || $carBlock.data('autoplay'),
                asNavv = $carBlock.find('.carousel-as-nav').length ? $carBlock.find('.carousel-as-nav-inner') : '';
            //
            // slick slider settings
            //
            var $carousel = $carBlock.find('.carousel-inner'),
                $slider = $carousel.slick({
                    arrows: false,
                    slidesToShow: $s2show.bd,
                    autoplay: autoplay,
                    asNavFor: asNavv,
                    responsive: [{
                        breakpoint: 992,
                        settings: {
                            slidesToShow: $s2show.b1
                        }
                    },
                        {
                            breakpoint: 768,
                            settings: {
                                slidesToShow: $s2show.b2
                            }
                        },
                        {
                            breakpoint: 520,
                            settings: {
                                slidesToShow: 1
                            }
                        }
                    ]
                });
            //
            // find and set arrow click action
            //
            $carBlock.find('.carousel-arrow.left').click(function(event) {
                $slider.slick('slickPrev');
            });
            $carBlock.find('.carousel-arrow.right').click(function(event) {
                $slider.slick('slickNext');
            });
            //
            // find and set nav items
            //
            var $nav = $carBlock.find('.carousel-nav'),
                $navItem = $nav.find('.carousel-nav-item'),
                $isactive = $nav.find('.is-active');
            // set is-active nav-item if not defined
            if(!$isactive.length){
                $navItem.eq(0).addClass('is-active');
            }
            // change is-active nav-item on slide change
            $slider.on('afterChange', function(event, slick, currentSlide, nextSlide) {
                $navItem.siblings().removeClass('is-active');
                $navItem.eq(currentSlide).addClass('is-active');
            });
            // change slide on nav-item Click
            $navItem.each(function(index, el) {
                $(this).click(function(event) {
                    $slider.slick('slickGoTo', index);
                    var $navClickitem = $(this);
                    $slider.on('afterChange', function(event, slick, currentSlide, nextSlide) {
                        $navClickitem.siblings().removeClass('is-active');
                        $navItem.eq(currentSlide).addClass('is-active');
                    })
                });
            });
            //
            // find and set AsNavFor carousel
            //
            if($carBlock.find('.carousel-as-nav').length){
                var $asNavWrapper = $carBlock.find('.carousel-as-nav'),
                    $asNav = $carBlock.find('.carousel-as-nav-inner');
                $s2show = $asNavWrapper.data('s2show') ? breakpoints($asNavWrapper.data('s2show')) : breakpoints(6),
                    autoplay = false || $asNavWrapper.data('autoplay');
                // slider settings
                var $asNavSlider = $asNav.slick({
                    slidesToShow: $s2show.bd,
                    asNavFor: $slider,
                    centerMode: true,
                    arrows:false,
                    autoplay: autoplay,
                    focusOnSelect: true,
                    responsive: [{
                        breakpoint: 992,
                        settings: {
                            slidesToShow: $s2show.b1
                        }
                    },
                        {
                            breakpoint: 768,
                            settings: {
                                slidesToShow: $s2show.b2
                            }
                        },
                        {
                            breakpoint: 520,
                            settings: {
                                slidesToShow: 2
                            }
                        }
                    ]
                });
                //
                // AsNavFor carousel's arrow click actions
                //
                $asNavWrapper.find('.carousel-arrow.left').click(function(event) {
                    $asNavSlider.slick('slickPrev');
                });
                $asNavWrapper.find('.carousel-arrow.right').click(function(event) {
                    $asNavSlider.slick('slickNext');
                });
            }


        });
        //
        //  just to get variables for breakpoints
        //
        function breakpoints(n){
            var x = parseInt(n, 10),
                $breakpoints ={};
            if (x > 1) {
                $breakpoints.bd = x;
                $breakpoints.b1 = parseInt(x / 2, 10) + 1;
                $breakpoints.b2 = parseInt(x / 2, 10);
            } else {
                $breakpoints.bd = $breakpoints.b1 = $breakpoints.b2 = 1;
            }
            return $breakpoints;
        }
    }
    //  capacity-changer
    if($('.capacity-select').length){
        let $capacityBlocks =  $('.capacity-select');
        $capacityBlocks.each(function(index, el) {
            let $capacityBlock = $(this),
                $capacitySelected = $capacityBlock.find('.capacity-selected'),
                $capacityOptions  = $capacityBlock.find('.capacity-options-item');

            $capacityBlock.click(function(){
                if($(this).hasClass('is-active')){
                    $(this).removeClass('is-active');
                }else{
                    $(this).addClass('is-active');
                }
            });

            $capacityOptions.each(function(index, el) {

                $(this).click(function(event) {
                    event.stopPropagation();
                    let $capacityOption = $(this),
                        $text = $capacityOption.text();
                    price = $capacityOption.data('price');
                    $price = $capacityBlock.parent().find('.catalog-list-item-price');
                    $cartPrice = $capacityBlock.closest('.cart-table-row').find('.cart-table-cell-price span');

                    $cartPrice.text(price);
                    $price.text(price);
                    $capacitySelected.text($text);
                    $capacityBlock.removeClass('is-active');

                    checkCart();
                });
            });
        });
    }



    // quantiy-changer
    if($('.quantity-ctrl').length){
        let $quanBlocks = $('.quantity-ctrl');

        $quanBlocks.each(function(index, el){
            let $quanBlock = $(this),
                $digit = $quanBlock.find('.quantity-digit'),
                $incr = $quanBlock.find('.quantity-incr'),
                $dicr = $quanBlock.find('.quantity-dicr');

            $incr.click(function(){
                let incr = Number($digit.val());
                $digit.val(incr+1);
                checkCart();
            });
            $dicr.click(function(){
                let incr = Number($digit.val());
                if(incr !== 1){
                    $digit.val(incr-1);
                    checkCart();
                }
            })


        })
    }

    // cart check
    function checkCart(){
        if($('.cart-table-row-js').length){
            let $rows = $('.cart-table-row-js'),
                $final = $('.cart-final span');
            finalPrice = 0;
            $items = $('.cart-info-details .cart-info-quantity span span');
            $itemsText = $('.cart-info-details .cart-info-quantity span i');
            $infoSumm = $('.cart-info-details .cart-info-summ span span');

            $rows.each(function(index, el) {
                let $quantity = $(this).find('.quantity-digit'),
                    $price = $(this).find('.cart-table-cell-price span'),
                    $summ = $(this).find('.cart-table-cell-summ span');

                $summ.text( parseInt($quantity.val()) * parseInt($price.text()) );

                finalPrice += parseInt($summ.text());

            });
            $final.text(finalPrice);
            $infoSumm.text(finalPrice);
            $items.text($rows.length);
            $itemsText.text(declOfNum($rows.length, ['Ñ‚Ð¾Ð²Ð°Ñ€','Ñ‚Ð¾Ð²Ð°Ñ€Ð°','Ñ‚Ð¾Ð²Ð°Ñ€Ð¾Ð²',] ));
        }
    }

    // ÑÐºÐ»Ð¾Ð½ÐµÐ½Ð¸Ðµ Ñ‡Ð¸Ð»Ð¸Ñ‚ÐµÐ»ÑŒÐ½Ñ‹Ñ… (Ð¾Ñ„Ñ„Ð¸Ð³ÐµÐ½Ð½Ð°Ñ Ñ„ÑƒÐ½ÐºÑ†Ð¸Ñ)

    function declOfNum(number, titles) {
        cases = [2, 0, 1, 1, 1, 2];
        return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
    }

    // tab-changer

    if($('.tab-item').length){
        if(!$('.tab-wrap-link .tab-item.is-active').length){
            $('.tab-wrap-link .tab-item').eq(0).addClass('is-active');
        }
        if(!$('.tab-wrap-cont .tab-item-cont.is-active').length){
            $('.tab-wrap-cont .tab-item-cont').eq(0).addClass('is-active');
        }
        $('.tab-item').each(function(index){
            $(this).click(function(){
                $(this).addClass('is-active').siblings().removeClass('is-active');
                $('.tab-wrap-cont .tab-item-cont').eq(index).addClass('is-active').siblings().removeClass('is-active');
            })
        })
    }


    // mobile btn add
    if ($('#mobile-button').length) {
        var $btn = $('.mobile-button'),
            $menu = $('#mobile-menu'),
            $menuInner = $('#mobile-menu .mobile-menu-inner'),
            $overlay = $('.mobile-menu-overlay'),
            $collect = $('.collect-in');
        $collect.each(function(index, el) {
            var $clone = $(this).children().clone();
            $menuInner.append($clone);
        });
        $btn.click(function() {
            $overlay.toggleClass('open');
            $('body').toggleClass('open');
            $(this).toggleClass('open');
            $menu.toggleClass('open');
        });
        $overlay.click(function() {
            $btn.trigger('click');
        });
    }

});
