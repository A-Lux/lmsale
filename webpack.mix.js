const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    // JS
    .copy('resources/assets/js/app.js', 'public/js')
    .combine([
        'resources/assets/js/libs/jquery-3.1.1.min.js',
        'resources/assets/js/libs/jquery.fancybox.pack.js',
        'resources/assets/js/libs/slick.min.js',
    ], 'public/js/libs.js')

    // CSS
    .copy('resources/assets/css/libs/fancybox', 'public/css/libs')
    .copy('resources/assets/css/app.css', 'public/css')
    .combine([
        'resources/assets/css/libs/bootstrap.min.css',
        'resources/assets/css/libs/fancybox/jquery.fancybox.css',
        'resources/assets/css/libs/slick.css',
    ], 'public/css/libs.css')

    // Fonts
    .copy('resources/assets/fonts/**/*', 'public', false)

   // Backend
    .sass('resources/assets/backend/sass/app.scss', 'public/css/backend')
    .js('resources/assets/backend/js/app.js', 'public/js/backend')

    // Font-Awesome
    .copy('node_modules/font-awesome/fonts', 'public/css/backend/fonts')

    // Scripts
    .js('resources/assets/backend/js/libs/tinymce.js', 'public/js/backend/libs/tinymce.js')
    .js('resources/assets/backend/js/libs/onleave.js', 'public/js/backend/libs/onleave.js')

    // Libs
    .copy('resources/assets/backend/js/libs/jquery-ui.js', 'public/js/backend/libs/jquery-ui.js')
    .copy('resources/assets/backend/js/libs/datepicker.js', 'public/js/backend/libs/datepicker.js')
    .copy('resources/assets/backend/sass/libs/datepicker.css', 'public/css/backend/libs/datepicker.css')

    .version([
        'public/css/app.css',
        'public/js/app.js'
    ]);
